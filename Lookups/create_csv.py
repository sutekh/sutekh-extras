#!/usr/bin/env python

"""Combine the different lookup csv files into a single file
   """

import csv
import os


LOOKUP_DATA = ('CardNames.csv', 'CardTypes.csv', 'Clans.csv',
               'Creeds.csv', 'Disciplines.csv', 'Expansions.csv',
               'Rarities.csv', 'Sects.csv', 'Virtues.csv')
DATA_PATH = 'data'
OUTPUT = 'lookup.csv'

# We allow comments in the source csv files, since that makes maintaining
# them easier


def main():
    aComplete = []
    for sFileName in LOOKUP_DATA:
        sFullPath = os.path.join(DATA_PATH, sFileName)
        sDomain = os.path.splitext(sFileName)[0]
        oFile = open(sFullPath, 'r')
        oReader = csv.reader(oFile)
        aRows = list(oReader)
        oFile.close()
        for tRow in aRows:
            if tRow[0].startswith('# '):
                continue
            try:
                sLookupKey, sValue = tRow
            except Exception:
                print("Exception handling row %s from %s" % (tRow, sFileName))
                raise
            aComplete.append((sDomain, sLookupKey, sValue))

    oOutput = open(OUTPUT, 'w')

    writer = csv.writer(oOutput)
    writer.writerows(aComplete)
    oOutput.close()


if __name__ == "__main__":
    main()
