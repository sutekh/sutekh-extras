#!/usr/bin/env python

"""Script for downloading and cleaning up rulebooks and other HTML
   files for use in Sutekh.
   """

import urllib2
import os
import re
import logging
import StringIO
import html5lib
from xml.etree.ElementTree import SubElement
from PIL import Image
logger = logging.getLogger()
# logging.basicConfig(level=logging.DEBUG)


CACHE_FOLDER = "cache"
RULEBOOK_FOLDER = "rulebook-data"


def rulebook_folder(sFilename):
    return os.path.join(RULEBOOK_FOLDER, sFilename)


class RuleSource(object):

    # List of fPreCleaner(oSrc, sData) -> sCleanerData
    # functions to apply to data while it's still a string
    PRECLEANERS = []

    # List of fCleaner(oSrc, oElementTree) functions
    # to apply after the pre-cleaners have made the HTML
    # vaguely sane
    CLEANERS = []

    def __init__(self, sTitle, sFilename, sUrl):
        self._sTitle = sTitle
        self._sFilename = sFilename
        self._sUrl = sUrl

    def _fetch_cached_url(self, sUrl):
        sBase = re.sub('[/:.?#]', '_', sUrl.partition('://')[2])
        sCached = os.path.join(CACHE_FOLDER, sBase)
        if os.path.isfile(sCached):
            logger.info("Using cache for %s" % sUrl)
            return open(sCached).read()
        else:
            sData = urllib2.urlopen(sUrl).read()
            open(sCached, "w").write(sData)
            return sData

    def index_entry(self):
        return self._sFilename, self._sTitle

    def fetch(self):
        logger.info("Fetching %s from %s ...", self._sFilename, self._sUrl)
        return self._fetch_cached_url(self._sUrl)

    def clean(self, sData):
        for fPreCleaner in self.PRECLEANERS:
            sData = fPreCleaner(self, sData)

        oTree = html5lib.parse(sData, treebuilder="lxml",
                               namespaceHTMLElements=False)

        for fCleaner in self.CLEANERS:
            fCleaner(self, oTree)

        fOut = StringIO.StringIO()
        oTree.write(fOut, method="html")
        return fOut.getvalue()

    def write(self, sData):
        fOut = open(rulebook_folder(self._sFilename), "wb")
        fOut.write(sData)
        fOut.close()


#
# pre-cleaners
#

def regex_replace(*aReplaces):
    """Replace a given pattern with a given string."""
    def replace(oSrc, sData):
        for sPattern, sReplace in aReplaces:
            oRe = re.compile(sPattern,
                             flags=re.MULTILINE | re.IGNORECASE | re.DOTALL)
            sData = oRe.sub(sReplace, sData)
        return sData
    return replace


#
# cleaners
#

def standardise_header(oSrc, oTree):
    """Replace header with standard header."""
    oHead = oTree.find("head")
    oHead.clear()
    oTitle = SubElement(oHead, "title")
    oTitle.text = oSrc.index_entry()[1]


def strip_elements(*aPaths):
    """Strip out the tags listed in aPaths."""
    def strip(oSrc, oTree):
        for sPath in aPaths:
            logging.debug("Stripping elements matching %r", sPath)
            for oElem in oTree.xpath(sPath):
                oElem.getparent().remove(oElem)
                logging.debug("Stripped %r", oElem)
    return strip


def keep_only(sPath):
    """Strip out everything from under body except elements matching sPath."""
    def keep(oSrc, oTree):
        oBody = oTree.find("body")
        aKeep = oTree.xpath(sPath)
        oBody.clear()
        for oKeep in aKeep:
            oBody.append(oKeep)
    return keep


def make_toc(iStartLevel, iDepth):
    """Create a Table of Contents for the HTML from h? tags with ids.
       iStartLevel is the starting tag depth for the toc.
       iDepth is how many levels to add to the TOC.
       """
    def toc(oSrc, oTree):
        oTocList = None
        aTocLists = []
        aTags = [('h%d' % x) for x in range(iStartLevel, iStartLevel + iDepth)]
        for oElem in oTree.xpath('//*'):
            # Inefficient, but preserves ordering
            if 'id' in oElem.attrib and oElem.tag in aTags:
                iMyDepth = aTags.index(oElem.tag)
                if oTocList is None:
                    oPar = oElem.getparent()
                    oTocHeader = SubElement(oPar, 'h2')
                    # This is a bit funky, but we need to move the
                    # list to before the header, and lxml's type
                    # checking makes using Element directly tricky
                    oPar.remove(oTocHeader)
                    oPar.insert(0, oTocHeader)
                    oTocHeader.text = 'Contents'
                    oTocHeader.tail = '\n'  # pretty printing
                    oTocList = SubElement(oPar, 'ul')
                    oPar.remove(oTocList)
                    oPar.insert(1, oTocList)
                    oTocList.tail = '\n'
                # Need to determine depth of the element
                if iMyDepth == 0:
                    oMyTocList = oTocList
                    aTocLists = []
                elif iMyDepth == iLastDepth:
                    oMyTocList = oLastTocList
                elif iMyDepth > iLastDepth:
                    oMyTocList = SubElement(oLastTocList, 'ul')
                    oMyTocList.tail = '\n'
                    aTocLists.append(oLastTocList)
                else:
                    # Need to backtrack one level
                    oMyTocList = aTocLists.pop()
                oTocEntry = SubElement(oMyTocList, 'li')
                oTocLink = SubElement(oTocEntry, 'a')
                oTocLink.attrib['href'] = '#%s' % oElem.attrib['id']
                oTocLink.text = oElem.text
                oTocEntry.tail = '\n'
                iLastDepth = iMyDepth
                oLastTocList = oMyTocList
    return toc


def fix_images(sSitePrefix, sImageBase):
    """Fix image tages in the source, fetching and replacing those that
       match sImageBase, and removing those that don't"""

    def resize(oElem, sFilename, oImgData):
        sFilename = rulebook_folder(sFilename)
        try:
            iW = int(oElem.attrib['width'])
            iH = int(oElem.attrib['height'])
        except KeyError as e:
            # Can't resize, so bail
            logger.info("Unable to resize %r due to missing size information",
                        sFilename)
            return oImgData
        fImg = StringIO.StringIO(oImgData)
        # Pillow needs RGBA here
        oImg = Image.open(fImg).convert("RGBA")  # convert to RGBA for resize
        oResizedImg = oImg.resize((iW, iH), Image.ANTIALIAS)
        if 'jpg' in sFilename:
            # Need to drop alpha to save jpgs
            oResizedImg = oResizedImg.convert("RGB")
        fResizedImg = StringIO.StringIO()
        fResizedImg.name = sFilename
        oResizedImg.save(fResizedImg)
        logger.info("Resized %r to %d x %d", sFilename, iW, iH)
        return fResizedImg.getvalue()

    def process_images(oSrc, oTree):
        """Do the actual handling"""
        for oElem in oTree.xpath('//img'):
            if oElem.attrib['src'].startswith(sImageBase):
                sUrl = sSitePrefix + oElem.attrib['src']
                oImgData = oSrc._fetch_cached_url(sUrl)
                sFilename = sUrl.split('/')[-1]  # last element
                oElem.attrib['src'] = sFilename
                oImgData = resize(oElem, sFilename, oImgData)
                fOut = open(rulebook_folder(sFilename), "wb")
                fOut.write(oImgData)
                fOut.close()
            else:
                # String it
                oElem.getparent().remove(oElem)
    return process_images


#
# source types
#

class Vekn(RuleSource):

    CLEANERS = [
        standardise_header,
        strip_elements(
            "//img",
            "//script",
            "//td[@class=\"buttonheading\"]",
            "//div[@class=\"moduletable\"]",
            "//div[@class=\"moduletable_menu\"]",
            "//div[@class=\"icons\"]",
            ),
        ]


class Lasombra(RuleSource):

    PRECLEANERS = [
        regex_replace(
            # Remove wayback toolbar cruft
            (r'<!--.*-->', ''),
            # remove background tag
            (r'background="[^*]*"', '')
            ),
        ]

    CLEANERS = [
        standardise_header,
        strip_elements(
            "//img",
            "//script",
            ),
        ]


class Tripod(RuleSource):

    PRECLEANERS = [
        regex_replace(
            # Remove comment cruft
            (r'<!--.*-->', ''),
            # Replace table wrappings
            (r'<table.*?<td>', ''),
            (r'</td>.*?</table>', ''),
            # Remove menu
            (r'<font color="black" size="\+1">.*?</font>', ''),
            (r'<center>.*</center>', '')
            ),
        ]

    CLEANERS = [
        standardise_header,
        strip_elements(
            "//img",
            "//script",
            "//style",
            "//div",
            ),
        ]


class VeknRulings(Vekn):

    PRECLEANERS = [
        regex_replace(
            (r"</body>.*?<body .*?>", ""),
            # Turn the relative links to the forum into absolute links
            (r'<a([^>]*) href="/index\.php/forum',
                r'<a \1 href="http://www.vekn.net/forum'),
            (r'<a([^>]*) href="/forum',
                r'<a \1 href="http://www.vekn.net/forum'),
            # Remove links from headers
            (r'<a href="/(card|general)-rulings[^>]*>([^<]*)</a>',
                r'\2'),
            # Standardise bold markers (needed for parser)
            (r'<strong>([^<]*)</strong>', r'<b>\1</b>'),
            # Fix corner case in the card-rulings
            (r'</b><b>:</b>', r':</b>'),
            ),
        ]

    def __init__(self, sTitle, sFilename, aUrls):
        super(VeknRulings, self).__init__(sTitle, sFilename, None)
        self._aUrls = aUrls

    def fetch(self):
        logger.info("Fetching %s from multiple URLs ...", self._sFilename)
        aData = []
        for sUrl in self._aUrls:
            logger.info("Downloading %s ...", sUrl)
            aData.append(self._fetch_cached_url(sUrl))
        return "\n".join(aData)


class VeknRulebook(RuleSource):

    # Pre-cleaners aim to fix the various headings so we can create a nice TOC
    PRECLEANERS = [
        regex_replace(
            # Add h1 tags around the page title
            (r'<div class="componentheading">([^<]*)</div>', r'<h1>\1</h1>'),
            # Remove pdf links
            (r'<li><a href="[^"]*pdf">.*?</li>', ''),
            # Turn the Section headings into proper h2 tags
            (r'<a href="/rulebook/([0-9]+-[^"]*)" '
             r'[^0-9]*([0-9]+\. [^<]*)</a>',
             r'<h2 id="\1">\2</h2>'),
            (r'<a href="/rulebook/([^"]*)" '
             r'[^a]*(appendix): ([^<]*)</a>',
             r'<h2 id="\1">\2 A: \3</h2>'),
            # Turn the subsection headings into h3 and h4 tags with an id.
            (r'<h2><strong>(?:<a name="[^"]*"></a>)?([0-9]+\.[0-9]+)\. '
                r'([^<]*)</strong></h2>',
             r'<h3 id="\1">\1 \2</h3>'),
            (r'<h2><strong>(?:<a name="[^"]*"></a>)?'
                r'([0-9]+\.[0-9]+\.[0-9]+)\. ([^<]*)</strong></h2>',
             r'<h4 id="\1">\1 \2</h4>'),
            (r'<h2 align="left"><b>(?:<a name="[^"]*"></a>)?([0-9]+\.[0-9]+)\.'
                r' ([^<]*)</b></h2>',
             r'<h3 id="\1">\1 \2</h3>'),
            (r'<h2 align="left"><b>(?:<a name="[^"]*"></a>)?'
                r'([0-9]+\.[0-9]+\.[0-9]?)\. ([^<]*)</b></h2>',
             r'<h4 id="\1">\1 \2</h4>'),
            # Add ids to the Imbued Appendix
            (r'<h2>([0-9])\. ([^<]*)</h2>', r'<h3 id="A.\1">A.\1. \2</h3>'),
            (r'<h2>([0-9])\. ([^<]*)<img [^>]*></h2>',
             r'<h3 id="A.\1">A.\1. \2</h3>'),
            (r'<h2>(Card Rulings)</h2>', r'<h3 id="A.6">A.6. \1</h3>'),
            # Fix links appearing in the text
            (r'<a href="/rulebook/[^#"]*#([0-9]\.[0-9])'
                r'\.[^0-9"]*">([^<]*)</a>',
             r'<a href="#\1">\2</a>'),
            (r'<a href="/rulebook/[^#>"]*#([0-9]\.[0-9]\.[0-9])'
                r'\.[^0-9"]*">([^<]*)</a>',
             r'<a href="#\1">\2</a>'),
            (r'<a href="#([0-9]\.[0-9])\.[^0-9"]*">([^<]*)</a>',
             r'<a href="#\1">\2</a>'),
            (r'<a href="/rulebook/appendix-imbued-rules">([^<]*)</a>',
             r'<a href="#appendix-imbued-rules">\1</a>'),
            # Strip links we can't easily fix
            (r'<a href="/rulebook/[^#"]*">([^<]*)</a>',
             r'\1'),
            (r'<a href="/rulebook/[^#"]*#[^0-9"]*">([^<]*)</a>',
             r'\1'),
            ),
        ]

    CLEANERS = [
        standardise_header,
        strip_elements(
            "//script",
            "//td[@class=\"buttonheading\"]",
            "//div[@class=\"moduletable\"]",
            "//div[@class=\"moduletable_menu\"]",
            "//div[@class=\"icons\"]",
            ),
        fix_images('http://www.vekn.net/', '/images/stories'),
        make_toc(2, 3),
        ]


RULEBOOKS = [
    VeknRulebook("Rulebook", "rulebook.html",
                 "http://www.vekn.net/rulebook"),
    Vekn("V:TES Detailed Play Summary", "detailed-play-summary.html",
         "http://www.vekn.net/detailed-play-summary"),
    VeknRulings("Rulings", "rulings.html", [
        "http://www.vekn.net/general-rulings",
        "http://www.vekn.net/card-rulings/card-rulings-a-to-f",
        "http://www.vekn.net/card-rulings/card-rulings-g-to-o",
        "http://www.vekn.net/card-rulings/card-rulings-p-to-z",
        ]),
    Lasombra("VTES FAQ", "faq.html",
             "https://web.archive.org/web/20131219020847/http://www.thelasombra.com:80/vtes_faq.htm"),
    Tripod("Imbued FAQ", "imbued_faq.html",
             "http://vtes-hunter-net.tripod.com/FAQ.htm"),
    Vekn("V:EKN Tournament Rules", "tournament_rules.html",
         "http://www.vekn.net/tournament-rules"),
    Vekn("Tournament Rating System", "rating_system.html",
         "http://www.vekn.net/rating-system"),
    Vekn("Judge's Guide", "judges_guide.html",
         "http://www.vekn.net/judges-guide"),
    ]


def main():
    if not os.path.isdir(CACHE_FOLDER):
        os.mkdir(CACHE_FOLDER)

    aIndexes = []
    for oRulebook in RULEBOOKS:
        sData = oRulebook.fetch()
        sData = oRulebook.clean(sData)
        oRulebook.write(sData)
        aIndexes.append(oRulebook.index_entry())

    fIndex = open(rulebook_folder("index.txt"), "w")
    for sTitle, sFilename in aIndexes:
        fIndex.write("%s:%s\n" % (sTitle, sFilename))
    fIndex.close()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
