#!/usr/bin/env python

"""Script for downloading and cleaning date csv files for use in Sutekh.
   """

import csv
import datetime
import logging
import json
import os
import sys

import yaml

logger = logging.getLogger()
# logging.basicConfig(level=logging.DEBUG)

sys.path.append('.')

from create_csv import (fetch_csv_files, parse_extra_data, make_cache_dir,
                        EXPANSION_FOLDER, EXTRA_DATA)

VARIANT_DATA = 'variants.yml'
JSON_FILE =  "expansions.json"

# Key is the back type
# Values are a tuple of specific expansions
SPECIFIC_BACKS_INFO = {
    'Jyhad': ('Jyhad', ),
    # Third Ed reversed backs
    # Also backs for Gran Madre di Dio, Italy & Kestrelle Hayes Promo cards
    'Third Edition': ('Third', 'Promo-20061126', 'Promo-20070101'),
    # Special storyline prints
    'Special': ('Nergal Storyline', 'Cultist Storyline'),
    # These are printed as part of the storyline event, so they get the PDF back
    # type
    'PDF': ('Fall of London Storyline Cards',),
}

# Key is the back type
DATE_BACK_INFO = {
    # Old WW VtES backs (Deckmaster logo)
    'VtES': (datetime.date(year=1995, month=9, day=15),
             datetime.date(year=2010, month=9, day=25)),
    # PDF sets released by the VEKN
    'PDF': (datetime.date(year=2012, month=5, day=31),
            datetime.date(year=2016, month=1, day=17)),
    # Black chantry releases
    # No Deckmaster logo
    # We use a maximum date of some weeks in the future to handle cases where the cardlist is
    # updated before the release date
    'Black Chantry': (datetime.date(year=2017, month=5, day=11),
                      datetime.date.today()+datetime.timedelta(weeks=12)),
}


def add_back_info(aExpansionList):
    aFullData = []
    for sExpName, sDate in aExpansionList:
        sBack = None
        oDate = datetime.datetime.strptime(sDate, '%Y%m%d').date()
        for sCandBack, tExpInfo in SPECIFIC_BACKS_INFO.items():
            if sExpName in tExpInfo:
                sBack = sCandBack
                break
        if not sBack:
            for sCandBack, tDateRange in DATE_BACK_INFO.items():
                dStart, dEnd = tDateRange
                if dStart <= oDate and dEnd >= oDate:
                    sBack = sCandBack
                    break
        if not sBack:
            raise RuntimeError("Failed to determine back for %s" % sExpName)
        aFullData.append((sExpName, oDate.strftime('%Y-%m-%d'), sBack))
    return aFullData

def add_variant_info(aExpList):
    sFile = os.path.join(EXPANSION_FOLDER, VARIANT_DATA)

    with open(sFile, 'r') as f:
        dVariantInfo = yaml.safe_load(f)

    dExpInfo = {}
    for sExpName, sDate, sBack in aExpList:
        # Check date format
        if sDate:
            try:
                datetime.datetime.strptime(sDate, "%Y-%m-%d")
            except ValueError as e:
                print("Invalid date format for %s" % sExpName)
                raise
        dExpInfo[sExpName] = {}
        dExpInfo[sExpName]["None"] = {}
        dExpInfo[sExpName]["None"]["back"] = sBack
        dExpInfo[sExpName]["None"]["date"] = sDate
        if sExpName in dVariantInfo:
            for sVariant, dInfo in dVariantInfo[sExpName].items():
                if 'date' in dInfo:
                    try:
                        datetime.datetime.strptime(dInfo['date'], "%Y-%m-%d")
                    except ValueError as e:
                        print("Invalid date format for %s (%s)" % (sExpName, sVariant))
                        raise
                dExpInfo[sExpName][sVariant] = dInfo
    return dExpInfo


def main():
    make_cache_dir()
    aComplete = []

    aDateData = fetch_csv_files()
    aDateData.extend(parse_extra_data())
    aDateData = sorted(set(aDateData), key=lambda x: x[1])
    aFullData = add_back_info(aDateData)

    dAllInfo = add_variant_info(aFullData)

    sOutput = os.path.join(EXPANSION_FOLDER, JSON_FILE)
    print(json.dumps(dAllInfo, sort_keys=True, indent=4,
                     separators=(', ', ': ')))
    with open(sOutput, 'w') as f:
        json.dump(dAllInfo, f)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
