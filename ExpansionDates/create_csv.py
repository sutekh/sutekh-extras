#!/usr/bin/env python

"""Script for downloading and cleaning up rulebooks and other HTML
   files for use in Sutekh.
   """

from urllib.request import urlopen
import os
import re
import logging
import csv
from io import StringIO, BytesIO
import zipfile 
logger = logging.getLogger()
# logging.basicConfig(level=logging.DEBUG)


CACHE_FOLDER = "cache"
EXPANSION_FOLDER = "expansion-data"
DATE_FILE = "expansiondates.csv"
CSV_URL = 'http://www.vekn.net/images/stories/downloads/vtescsv_utf8.zip'
EXTRA_DATA = 'Storyline.csv'
CRYPT_CSV_INDEX = 10
LIB_CSV_INDEX = 12


# A list of expansions we exluce because they are aliases for another expansion and having
# both in the output can lead to incorrect date information.
EXCLUDED = ['SP']


def get_promo_date(sExp):
    """Extract the promo date from the expansion"""
    sDate = sExp.split('-')[1]
    # Annoying inconsitency in the csv file
    if sDate.endswith(':'):
        sDate = sDate[:-1]
    return sDate


def process_promo(sExp):
    """Convert a promo entry into a promo name and a date"""
    if sExp.startswith('Promo-'):
        # Promo-YYYYMMDD[:Count]
        # Just extract the name and convert the date
        sExp = sExp.split(':')[0]
        sDate = get_promo_date(sExp)
    elif sExp.startswith('Promo:'):
        #  Promo:YYYYMMDD[Count]
        sExp = sExp.replace(':', '-')
        sExp = sExp[:14] # Strip count from name
        sDate = get_promo_date(sExp)
    return sExp, sDate


def fetch_csv_files():
    sBase = re.sub('[/:.?#]', '_', CSV_URL.partition('://')[2])
    sCached = os.path.join(CACHE_FOLDER, sBase)
    if os.path.isfile(sCached):
        logger.info("Using cache for %s" % CSV_URL)
        sData = open(sCached, 'rb').read()
    else:
        sData = urlopen(CSV_URL).read()
        open(sCached, "wb").write(sData)

    oZip = zipfile.ZipFile(BytesIO(sData))
    # extract the csv files to StringIO objects
    try:
        sCryptCSV = oZip.open('vtescsv/vtescrypt.csv').read().decode('utf8')
    except KeyError:
        sCryptCSV = oZip.open('vtescrypt.csv').read().decode('utf8')
    try:
        sLibCSV = oZip.open('vtescsv/vteslib.csv').read().decode('utf8')
    except KeyError:
        sLibCSV = oZip.open('vteslib.csv').read().decode('utf8')
    try:
        sSetsCSV = oZip.open('vtescsv/vtessets.csv').read().decode('utf8')
    except KeyError:
        sSetsCSV = oZip.open('vtessets.csv').read().decode('utf8')
    aInfo = []
    # Process sets info
    reader = csv.reader(StringIO(sSetsCSV))
    next(reader)
    for row in reader:
        # Order is ID, shortname, date, long name, company
        # We use shortname, date here to match abbrevations better
        if row:
            if row[1] in EXCLUDED:
                continue
            aInfo.append((row[1], row[2]))
    # Process crypt for promos
    reader = csv.reader(StringIO(sCryptCSV))
    next(reader)
    for row in reader:
        if not row:
            continue
        sExpInfo = row[CRYPT_CSV_INDEX]
        for sExp in sExpInfo.split(','):
            sExp = sExp.strip()
            if sExp.startswith('Promo'):
                sExp, sDate = process_promo(sExp)
                aInfo.append((sExp, sDate))
    # Process lib for promos
    reader = csv.reader(StringIO(sLibCSV))
    next(reader)
    for row in reader:
        if not row:
            continue
        sExpInfo = row[LIB_CSV_INDEX]
        for sExp in sExpInfo.split(','):
            sExp = sExp.strip()
            if sExp.startswith('Promo'):
                sExp, sDate = process_promo(sExp)
                aInfo.append((sExp, sDate))
    return aInfo


def parse_extra_data():
    sFile = os.path.join(EXPANSION_FOLDER, EXTRA_DATA)
    reader = csv.reader(open(sFile))
    aInfo = []
    next(reader)  # skip header
    for row in reader:
        # Order is shortname, date, long name, company
        # We use longname, date here since short names aren't well defined
        aInfo.append((row[2], row[1]))
    return aInfo


def make_cache_dir():
    if not os.path.isdir(CACHE_FOLDER):
        os.mkdir(CACHE_FOLDER)


def main():
    make_cache_dir()
    aComplete = []

    aDateData = fetch_csv_files()

    aDateData.extend(parse_extra_data())

    aDateData = sorted(set(aDateData), key=lambda x: (x[1], x[0]))

    oOutput = open(os.path.join(EXPANSION_FOLDER, DATE_FILE), 'w')

    writer = csv.writer(oOutput)
    writer.writerows(aDateData)
    oOutput.close()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
