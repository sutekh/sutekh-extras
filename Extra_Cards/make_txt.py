#!/usr/bin/env python
"""Simple script to generate WW'ish text file from text files of card info"""

import sys
import re

KEYWORDS = ['Cardtype', 'Clan', 'Group', 'Capacity', 'Cost', 'Discipline']

inf_discipline = re.compile('^\[[a-z][a-z][a-z]\]')
sup_discipline = re.compile('^\[[A-Z][A-Z][A-Z]\]')

def gen_text_for_card(dData, sStoryline, bLegal):
    """Create the text for card"""
    sType = 'Unknown' # used later to print Actions correctly
    # Output the card data
    sName = dData['name']
    # We re-encode to utf8 to ensure things so all strings to acoid implict
    # conversions to ascii screwing us
    sText = 'Name: %s\n' % sName
    if ':' not in sStoryline:
        sText += '[%s:Storyline]\n' % sStoryline
    else:
        sText += '[%s]\n' % sStoryline
    for sKey in KEYWORDS:
        if sKey in dData:
            sText += '%s: %s\n' % (sKey, dData[sKey])

    sText += '\n'.join(dData['text'])

    if not bLegal:
        sText += '\n{NOT FOR LEGAL PLAY}'
    if 'Artist' in dData:
        sText += '\nArtist: %s' % dData['Artist']
    sText += '\n'
    return sText

def convert(sFilename, sStoryline, dAllCards, bLegal):
    """Process the given text file, extracting the cards"""
    fTextFile = open(sFilename)
    dCard = {}
    for sLine in fTextFile:
        sLine = sLine.strip()
        if sLine.startswith('#'):
            continue
        if len(sLine) == 0:
            if dCard:
                dAllCards[dCard['name']] = gen_text_for_card(dCard, sStoryline,
                    bLegal)
                dCard = {}
            continue
        if 'name' not in dCard:
            dCard['name'] = sLine
            continue
        sKeyword = sLine.split(':')[0]
        if sKeyword in KEYWORDS + ['Artist']:
            dCard[sKeyword] = sLine.split(':', 1)[1].strip()
        else:
            if 'text' in dCard:
                dCard['text'].append(sLine)
            else:
                dCard['text'] = [sLine]
    # Final write if there wasn't a trailing newline in the file
    if dCard:
        dAllCards[dCard['name']] = gen_text_for_card(dCard, sStoryline, bLegal)
    fTextFile.close()


def add_exp(sCard, sExpansion, dAllCards):
    """Add a card and expansion to the card list"""
    oCurExp = re.compile('\[(.*)\]')
    if sCard in dAllCards:
        sCurText = dAllCards[sCard]
        oMatch = oCurExp.search(sCurText)
        # FIXME: Error handling for this case
        sNewExpansions = '[%s, %s]' % (oMatch.groups()[0], sExpansion)
        sText = oCurExp.sub(sNewExpansions, sCurText, 1)
    else:
        sText = 'Name: %s\n' % sCard
        sText += '[%s]\n' % sExpansion
    dAllCards[sCard] = sText


def create_exp_cards(sFileName, sExpansion, dAllCards):
    """Create text entries with the given expansion"""
    fTextFile = open(sFileName)
    for sCard in fTextFile:
        if not sCard.strip():
            continue
        if sCard.startswith('#'):
            continue
        sCard = sCard.strip()
        add_exp(sCard, sExpansion, dAllCards)
    fTextFile.close()


def create_promo_cards(sFileName, dAllCards):
    """The promo list includes the promo dates and comments.
       so we need to parse the file differentlyy"""
    fTextFile = open(sFileName)
    for sLine in fTextFile:
        if not sLine.strip():
            continue
        if sLine.startswith('#'):
            continue
        # We use ' ; ' separation to make this easier to parse
        if not ' ; ' in sLine:
            # This is probably an error, so flag it
            print("Non-emmpty, Non-comment line with no separator - skipping")
            print(f"   {sLine.strip()}")
            continue
        sCard, sExpansion = [x.strip() for x in sLine.split(' ; ')]
        add_exp(sCard, sExpansion, dAllCards)
    fTextFile.close()


def create_extra_keyword_cards(sFileName, sKeyword, dAllCards):
    """Create text entries with the given keyword"""
    oCardNameExp = re.compile('Name: (.*\n\[.*\])')
    fTextFile = open(sFileName)
    for sCard in fTextFile:
        if not sCard.strip():
            continue
        sCard = sCard.strip()
        if sCard in dAllCards:
            sCurText = dAllCards[sCard]
            oMatch = oCardNameExp.search(sCurText)
            # Insert keyword at start of the string
            sNewText = 'Name: %s\nKeywords: %s' % (oMatch.groups()[0], sKeyword)
            sText = oCardNameExp.sub(sNewText, sCurText)
        else:
            # Blank expansion is needed for the WW text parser
            sText = 'Name: %s\n[]\n' % sCard.strip()
            sText += 'Keywords: %s\n' % sKeyword
        dAllCards[sCard] = sText
    fTextFile.close()


def print_card_dict(dAllCards):
    """Print the cards correctly sorted"""
    for sKey in sorted(dAllCards):
        print(dAllCards[sKey])

def write_card_dict(dAllCards, sFile):
    """Print the cards correctly sorted"""
    with open(sFile, 'w') as oFile:
        for sKey in sorted(dAllCards):
            oFile.write(dAllCards[sKey])
            oFile.write('\n')  # Add seperating line

if __name__ == "__main__":
    dAllCards = {}
    if len(sys.argv) > 1:
        if sys.argv[1] == '--help' or sys.argv[1] == '-h':
            print(f"usage: {sys.argv[0]} [output file]")
            print()
            print("If [output file] is blank, this prints everything to stdout")
            sys.exit(0)
    # Special Storyline cards
    convert("Nergal_cards", "Nergal Storyline", dAllCards, bLegal=False)
    convert("Cultist_cards", "Cultist Storyline", dAllCards, bLegal=False)
    convert("Motivation_cards", "Eden's Legacy Storyline",
            dAllCards, bLegal=False)
    convert("EC_2013_Storyline", "Ragnarok - the Final Battle (EC 2013)",
            dAllCards, bLegal=False)
    convert("EC_2012_Storyline", "The Battle for Budapest (EC 2012)",
            dAllCards, bLegal=False)
    convert("NAC_2012_Storyline", "March of the War Ghouls (NAC 2012)",
            dAllCards, bLegal=False)
    convert("VEKN_2014_Mythic", "VEKN 2014 The Returned",
            dAllCards, bLegal=False)
    convert("VEKN_2014_Nictuku", "VEKN 2014 Rise of the Nictuku",
            dAllCards, bLegal=False)
    convert("VEKN_2014_Bardo", "VEKN 2014 Children of Osiris",
            dAllCards, bLegal=False)
    convert("VEKN_2015_RedSign", "VEKN 2015 The Red Sign",
            dAllCards, bLegal=False)
    convert("VEKN_2017_Ascension_of_Caine", "Ascension of Caine (EC 2017)",
            dAllCards, bLegal=False)
    convert("BC_2022_FoL_Storyline", "Fall of London Storyline Cards",
            dAllCards, bLegal=False)
    # Extra Expansions for cards
    create_exp_cards('NOR_cards', 'NoR:Rules', dAllCards)
    create_exp_cards('AaA_cards', 'Anarchs and Alastors Storyline', dAllCards)
    create_exp_cards("VEKN_2014_Nictuku_extras",
                     "VEKN 2014 Rise of the Nictuku:Storyline", dAllCards)
    create_exp_cards("WW_2003_Demo_Cards", "White Wolf 2003 Demo:Demo",
                     dAllCards)
    create_exp_cards("BC_Missing_PoD_Reprints", "POD:DTC",
                     dAllCards)
    create_exp_cards("BC_2024_Draft_Set", "draft2024:Fixed", dAllCards)
    # Promo cards
    # 2023 League promos - Date taken from Black Chantry Email
    create_exp_cards("2023_League_Promos", "Promo-20230926", dAllCards)
    # 2023 Event promos - date taken from BC site
    create_exp_cards("2023_Event_Promos", "Promo-20231213", dAllCards)
    # 2024 League promos - Date taken from Black Chantry Email
    create_exp_cards("2024_League_Promos", "Promo-20240627", dAllCards)

    # Misc Individual Promos
    create_promo_cards("Promo_Cards", dAllCards)

    # Add keywords
    create_extra_keyword_cards('RT_Banned', 'RT:Banned', dAllCards)
    create_extra_keyword_cards('RT_Watchlist', 'RT:Watchlist', dAllCards)
    create_extra_keyword_cards('EC_2013_Rewards', 'reward', dAllCards)
    if len(sys.argv) > 1:
        write_card_dict(dAllCards, sys.argv[1])
    else:
        print_card_dict(dAllCards)
