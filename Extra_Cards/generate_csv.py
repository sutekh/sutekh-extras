#!/usr/bin/env python
"""Script to create csv files from the data

   This reads the data from a sutekh database."""

import sys
import os
import sutekh
from sutekh.core import Filters
from sutekh.base.core.BaseObjects import AbstractCard, IAbstractCard
from sutekh.core.SutekhObjects import IDisciplinePair
from sutekh.base.Utility import prefs_dir, sqlite_uri
from sutekh.SutekhUtility import format_text
import optparse
import csv


class WWCVSDialect(csv.Dialect):
    """Setup csv writer to match the WW csv files"""
    delimiter = ','
    quotechar = '"'
    escapechar = None
    doublequote = True
    skipinitialspace = False
    lineterminator = '\n'
    quoting = csv.QUOTE_NONNUMERIC

csv.register_dialect('ww', WWCVSDialect)


def parse_options(aArgs):
    """Handle commandline arguments"""
    oOptParser = optparse.OptionParser(usage="usage: %prog [options]",
             version="%%prog 0.1.0")
    oOptParser.add_option("-d", "--db",
             type="string", dest="db", default=None,
             help="Database URI. [sqlite://$PREFSDIR$/sutekh.db]")
    oOptParser.add_option("-p", "--prefix",
             type="string", dest="prefix", default="storyline_cards_",
             help="Prefix for the outfile files. This will write files called "
             "$PREFIX$crypt.csv and $PREFIX$lib.csv")
    return oOptParser, oOptParser.parse_args(aArgs)


def fix_text(sText):
    """Fix text to right encoding and normalise disciplines and the
       'not for legal play' string"""
    # normalise the discipline markers in card text
    sText = format_text(sText)
    # Normalise NOT FOR LEGAL PLAY marker
    sText = sText.replace(' {NOT FOR LEGAL PLAY}', 'NOT FOR LEGAL PLAY')
    sText = sText.replace('{NOT FOR LEGAL PLAY}', 'NOT FOR LEGAL PLAY')
    sText = sText.replace('.NOT FOR', '.\nNOT FOR')
    return sText.encode('cp1251')


def get_discipline_flags(oCard):
    """Get the discipline flags needed for the csv file"""

    aFlags = []
    for sDiscipline in ['Abombwe', 'Animalism', 'Auspex', 'Celerity',
            'Chimerstry', 'Daimoinon', 'Dementation', 'Dominate',
            'Fortitude', 'Melpominee', 'Mytherceria', 'Necromancy', 'Obeah',
            'Obfuscate', 'Obtenebration', 'Potence', 'Presence', 'Protean',
            'Quietus', 'Sanguinus', 'Serpentis', 'Spiritus', 'Temporis',
            'Thanatosis', 'Thaumaturgy', 'Valeren', 'Vicissitude',
            'Visceratika']:
        oInfDiscipline = IDisciplinePair((sDiscipline, 'inferior'))
        oSupDiscipline = IDisciplinePair((sDiscipline, 'superior'))
        if oInfDiscipline in oCard.discipline:
            aFlags.append(1)
        elif oSupDiscipline in oCard.discipline:
            aFlags.append(2)
        else:
            aFlags.append(0)

    return aFlags


def write_crypt_csv(aCards, sPrefix):
    sFilename = sPrefix + 'crypt.csv'
    sHeader = "Name,Type,Clan,Adv,Group,Capacity,Disciplines,Card Text," \
            "Set,Title,Banned,Artist,Abombwe,Animalism,Auspex,Celerity," \
            "Chimerstry,Daimoinon,Dementation,Dominate,Fortitude,Melpominee," \
            "Mytherceria,Necromancy,Obeah,Obfuscate,Obtenebration,Potence," \
            "Presence,Protean,Quietus,Sanguinus,Serpentis,Spiritus,Temporis," \
            "Thanatosis,Thaumaturgy,Valeren,Vicissitude,Visceratika\n"

    oFile = open(sFilename, "w")
    oFile.write(sHeader)
    oWriter = csv.writer(oFile, dialect='ww')

    for oCard in sorted(aCards, key=lambda x: x.name):
        sName = oCard.name.encode('cp1252')
        sType = "/".join([x.name for x in oCard.cardtype])
        sAdvanced = ''
        if oCard.group:
            sGroup = "%d" % oCard.group
        else:
            sGroup = ""
        sCapacity = "%d" % oCard.capacity
        if oCard.clan:
            sClan = "/".join([x.name for x in oCard.clan])
        else:
            sClan = ''

        if oCard.discipline:
            aDisciplines = []
            aDisciplines.extend(sorted([oP.discipline.name for oP in
                oCard.discipline if oP.level != 'superior']))
            aDisciplines.extend(sorted([oP.discipline.name.upper() for oP in
                oCard.discipline if oP.level == 'superior']))
            sDisciplines = ' '.join(aDisciplines)
        else:
            sDisciplines = ''


        sText = fix_text(oCard.text)

        # Storyline cards have only 1 expansion
        oRarityPair = oCard.rarity[0]
        sSet = "%s:%s" % (oRarityPair.expansion.name, oRarityPair.rarity.name)

        sTitle = ""

        sBanned = ''
        if oCard.artists:
            sArtist = ";".join([x.name for x in oCard.artists])

        aDisciplineFlags = get_discipline_flags(oCard)

        aRow = [sName, sType, sClan, sAdvanced, sGroup, sCapacity,
            sDisciplines, sText, sSet, sTitle, sBanned, sArtist] + \
            aDisciplineFlags

        oWriter.writerow(aRow)

    oFile.close()


def write_library_csv(aCards, sPrefix):
    sFilename = sPrefix + 'lib.csv'
    sHeader = "Name,Type,Clan,Discipline,Pool Cost,Blood Cost," \
            "Conviction Cost,Burn Option,Card Text,Flavor Text,Set," \
            "Requirement,Banned,Artist\n"

    oFile = open(sFilename, "w")
    oFile.write(sHeader)
    oWriter = csv.writer(oFile, dialect='ww')

    for oCard in sorted(aCards, key=lambda x: x.name):
        sName = oCard.name.encode('cp1252')
        sType = "/".join([x.name for x in oCard.cardtype])
        if oCard.clan:
            sClan = "/".join([x.name for x in oCard.clan])
        else:
            sClan = ''
        if oCard.discipline:
            sDiscipline = " & ".join(x.discipline.fullname for x in oCard.discipline)
        else:
            sDiscipline = ''

        sBlood = ''
        sPool = ''
        # No conviction costs
        sConviction = ''
        # No burn option cards in storyline decks
        sBurn = ''
        if oCard.costtype == 'pool':
            sPool = '%d' % oCard.cost
        elif oCard.costtype == 'blood':
            sBlood = '%d' % oCard.cost

        sText = fix_text(oCard.text)

        sFlavourText = ''

        # Storyline cards have only 1 expansion
        oRarityPair = oCard.rarity[0]
        sSet = "%s:%s" % (oRarityPair.expansion.name, oRarityPair.rarity.name)

        sRequirement = ''
        sBanned = ''
        if oCard.artists:
            sArtist = ";".join([x.name for x in oCard.artists])

        oWriter.writerow([sName, sType, sClan, sDiscipline, sPool,
            sBlood, sConviction, sBurn, sText, sFlavourText, sSet,
            sRequirement, sBanned, sArtist])

    oFile.close()


def main(aArgs):
    oOptParser, (oOpts, aProcessedArgs) = parse_options(aArgs)
    sPrefsDir = prefs_dir("Sutekh")

    if len(aProcessedArgs) != 1:
        oOptParser.print_help()
        return 1

    if oOpts.db is None:
        oOpts.db = sqlite_uri(os.path.join(sPrefsDir, "sutekh.db"))

    sutekh.start(['sutekh', '-d', oOpts.db])

    oStorylineFilter = Filters.MultiExpansionFilter([
        "Eden's Legacy Storyline",
        "Nergal Storyline",
        "Cultist Storyline",
        ])

    oCryptFilter = Filters.FilterAndBox([
        Filters.CardTypeFilter('Vampire'),
        oStorylineFilter])

    oLibraryFilter = Filters.FilterAndBox([
        Filters.FilterNot(Filters.CardTypeFilter('Vampire')),
        oStorylineFilter])

    aCrypt = [IAbstractCard(x) for x in oCryptFilter.select(AbstractCard)]
    aLibrary = [IAbstractCard(x) for x in oLibraryFilter.select(AbstractCard)]

    write_crypt_csv(aCrypt, oOpts.prefix)
    write_library_csv(aLibrary, oOpts.prefix)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
