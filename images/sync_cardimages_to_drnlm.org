#!/bin/bash
CURDIR=$(dirname $0)
DATA=$(readlink -f $CURDIR/..)
cd $DATA
rsync -avr --del . vtesimages@drnlm.org:~/source/cardimages/
echo "Run the publish_images command on drnlm.org to export the images"
