
import sys
import math
import os

import numpy

import skimage.io
import skimage.color
import skimage.feature
import skimage.transform
import skimage.measure

import matplotlib.pyplot as plt

# These are adjusted for the padding we do
EXPECTED_POSITIONS = []
for x in (600, 1400, 2200):
    for y in (700, 1850, 3000):
        EXPECTED_POSITIONS.append((x, y))

DEBUG = False


def prepare_template():
    file_path = os.path.join(os.path.dirname(sys.argv[0]), 'template.png')
    base_template = skimage.io.imread(file_path, True)

    return base_template

def process_image(image, base_template):
    gray_image = skimage.color.rgb2gray(image)

    output = skimage.feature.match_template(gray_image, base_template, pad_input=True)

    peaks = skimage.feature.peak_local_max(output, min_distance=300)

    # Find the peaks closest to the expected positions
    closest = []
    for x, y in EXPECTED_POSITIONS:
        dist = 500
        cur_closest = (-1, -1)
        # peaks results is ordered by row / column, not x, y
        for yy, xx in peaks:
            if (xx, yy) in closest:
                continue
            cur_dist = math.sqrt((x-xx)**2 + (y-yy)**2)
            if cur_dist < dist:
                dist = cur_dist
                cur_closest = (xx, yy)
        if cur_closest != (-1, -1):
            closest.append(cur_closest)

    if DEBUG:
        for x, y in closest:
            print(x, y, output[y, x])
            for k in range(15):
                for l in range(15):
                    output[y-k, x-l] = 0.99
                    output[y-k, x+l] = 0.99
                    output[y+k, x-l] = 0.99
                    output[y+k, x+l] = 0.99

        f = plt.figure()
        f1 = plt.subplot(1, 2, 1)
        f2 = plt.subplot(1, 2, 2)
        f1.imshow(gray_image, cmap=plt.cm.gray)
        f2.imshow(output)
        plt.show()

    # cut the images
    result = {}
    cropped = {}
    index = 0
    margins = (580, 430)
    for x, y in closest:
        index += 1
        if y - margins[0] < 0:
            y -= (y - margins[0])
        if x - margins[1] < 0:
            x -= (x - margins[1])
        cropped[index] = image[y-margins[0]:y+margins[0], x-margins[1]:x+margins[1], :]

    for i in cropped:
        gray = skimage.color.rgb2gray(cropped[i])
        for sigma in numpy.arange(12, 8, -0.25):
            edges = skimage.feature.canny(gray, sigma=sigma)
            # We use -5 degrees to 5 degrees to find the vertical lines - (pi/180*5 = pi/36)
            # and use .1 degree increments, so 100 samples
            hspace, angles, distance = skimage.transform.hough_line(edges, numpy.linspace(-numpy.pi/36, numpy.pi/36, 100))
            peaks = list(zip(*skimage.transform.hough_line_peaks(hspace, angles, distance)))
            if len(peaks) >= 2:
                break
        if len(peaks) < 2:
            print("Unable to find lines for image", i)
            continue
        # We averge the two angles for a small bit of noise reduction - this is
        # probably not needed, but may be useful
        rot_angle = (peaks[0][1] * 180/numpy.pi + peaks[1][1] * 180/numpy.pi) / 2
        rotated = skimage.transform.rotate(cropped[i], rot_angle)

        # Need to calculate centroid of rotated image for final crop
        gray_rot = skimage.color.rgb2gray(rotated)
        rot_edges = skimage.feature.canny(gray, sigma=sigma)
        labelled = skimage.measure.label(rot_edges)
        props = skimage.measure.regionprops(labelled)

        row, col = numpy.round(props[0].centroid)

        if int(row)-52 < 0 or int(col)-370 < 0:
            print("Extracted region out of bounds for image", i)
            print(int(row) - 52, int(col) - 370)
            continue

        if int(row)+525 >= 2*margins[0] or int(col)+371 > 2*margins[1]:
            print("Extracted region out of bounds for image", i)
            print(int(row) + 525, 2*margins[0], int(col) + 371, 2*margins[1])
            continue

        final_cropped = rotated[int(row)-524:int(row)+525, int(col)-370:int(col)+371]

        if DEBUG:
            f = plt.figure()
            f1 = plt.subplot(2, 4, 1)
            f2 = plt.subplot(2, 4, 2)
            f3 = plt.subplot(2, 4, 3)
            f4 = plt.subplot(2, 4, 4)
            f5 = plt.subplot(2, 4, 5)
            f6 = plt.subplot(2, 4, 6)
            f7 = plt.subplot(2, 4, 7)
            f8 = plt.subplot(2, 4, 8)
            f1.imshow(gray)
            f2.imshow(edges)
            f3.imshow(numpy.log(1 + hspace), cmap=plt.cm.gray)
            f4.imshow(gray)
            rot_angle = 0
            for _, ang, dist in peaks:
                (x0, y0) = dist * numpy.array([numpy.cos(ang), numpy.sin(ang)])
                f4.axline((x0, y0), slope=numpy.tan(ang + numpy.pi/2))
            f5.imshow(rotated)
            f6.imshow(rot_edges)
            f7.imshow(labelled)
            f7.imshow(final_cropped)
            plt.show()

        result[i] = skimage.transform.resize(final_cropped, (704, 500), anti_aliasing=True)

    if DEBUG:
        f = plt.figure()
        for i in result:
            sf = plt.subplot(3, 3, i)
            sf.imshow(result[i])

        plt.show()

    return result


def main(scan_name, rotate, output_prefix):
    image = skimage.io.imread(scan_name)

    if rotate:
        # Because this is a 180 rotation, we could be much better, but
        # we're using rotate anyway elsewhere and the gain isn't that great.
        image = skimage.transform.rotate(image, 180.0)

    image = skimage.util.img_as_float(numpy.pad(image, ((100, 100), (100, 100), (0,0)), mode='maximum'))

    base_template = prepare_template()

    output = process_image(image, base_template)

    for i, img in output.items():
        new_name =  f'{prefix}_{i}.jpg'
        out_img = skimage.util.img_as_ubyte(img)
        skimage.io.imsave(new_name, out_img)


def usage():
    print("Usage: ")
    print(f"{sys.argv[0]}  <image name> [-r] [-p prefix] [--help]")
    print()
    print("Cuts a scanneed image of up to 9 cards into individual card scans.")
    print("This makes a lot of assumptions about the layout, and is intended to work on scans")
    print("With the cards in a 9-card sleeve to hold them in the correct positions and assumes")
    print("a scan resolution of 300 dpi.")
    print()
    print("Parameters:")
    print("<image name> - name of the image to cut (required)")
    print("-r -- specify that the scans need to be rotated 180 degrees")
    print("-p -- provide a prefix for the output images - defaults to the base name of the source image")
    print("--help -- display this help text")


if __name__ == "__main__":
    # Very fragile
    if len(sys.argv) < 2 or '--help' in sys.argv:
        usage()
        sys.exit(0)
    scan_name = sys.argv[1]
    rotate = False
    prefix = os.path.splitext(os.path.basename(scan_name))[0]
    if '-r' in sys.argv:
        rotate = True
    if '-p' in sys.argv:
        i = sys.argv.index('-p')
        prefix = sys.argv[i+1]

    main(scan_name, rotate, prefix)
        

