import sys

from html5lib.html5parser import parse

from urllib.request import urlopen, Request

def fix_name(name):
    name = name.lower()
    name = name.replace('group ', '0')
    for char in (" ", ".", ",", "'", "(", ")", "-", ":", "!", '"', "/"):
        name = name.replace(char, '')
    # This hack will work until we hit group 10
    return name + '.jpg'

def download(url, name):
    req = Request(url)
    req.add_header('User-Agent', 'Sutekh Image Plugin')
    with open(name, 'wb') as outfile:
        with urlopen(req) as httpstream:
            outfile.write(httpstream.read())


def process(table):
    """Pull out the image urls and card names from the table"""
    rows = table.findall('.//tr')
    for x in rows:
        img = x.find('.//img')
        if img is None:
            continue
        url = img.get('src').replace('-thumb140', '')
        ahrefs = x.findall('.//a')
        for cand in ahrefs:
            if not cand.text:
                continue
            origname = cand.text
            # We include space so we don't split on '-' in the card name
            cardname = origname.split(' - ')[1].strip()
        if 'Crypt' in origname:
            cardname = cardname.replace('[', '(Group ').replace(']',')')
        imagename = fix_name(cardname)
        if cardname.endswith(', The'):
            cardname = 'The '+ cardname.replace(', The', '')
        if cardname.endswith(', An'):
            cardname = 'An '+ cardname.replace(', An', '')
        download(url, imagename)
        print(cardname)


if __name__ == "__main__":
    html = parse(open(sys.argv[1], 'r'), namespaceHTMLElements=False)

    tables = html.findall('.//table')
    for x in tables:
        if x.get('class') == "bundle_contents":
            process(x)
            break

