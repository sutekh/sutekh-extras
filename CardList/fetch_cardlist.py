#!/usr/bin/env python

"""Script for downloading and cleaning up the official VEKN cardlist
   file for use in Sutekh.
   """

from urllib.request import urlopen
import os
import re
import logging
logger = logging.getLogger()
# logging.basicConfig(level=logging.DEBUG)


CACHE_FOLDER = "cache"
CARDLIST_FOLDER = "cardlist-data"


def cardlist_folder(sFilename):
    return os.path.join(CARDLIST_FOLDER, sFilename)


class CardListSource(object):

    PRECLEANERS = []

    def __init__(self, sTitle, sFilename, sUrl):
        self._sTitle = sTitle
        self._sFilename = sFilename
        self._sUrl = sUrl

    def _fetch_cached_url(self, sUrl):
        sBase = re.sub('[/:.?#]', '_', sUrl.partition('://')[2])
        sCached = os.path.join(CACHE_FOLDER, sBase)
        if os.path.isfile(sCached):
            logger.info("Using cache for %s" % sUrl)
            return open(sCached, 'rb').read()
        else:
            sData = urlopen(sUrl).read()
            open(sCached, "wb").write(sData)
            return sData

    def index_entry(self):
        return self._sFilename, self._sTitle

    def fetch(self):
        logger.info("Fetching %s from %s ...", self._sFilename, self._sUrl)
        return self._fetch_cached_url(self._sUrl)

    def clean(self, sData):
        for fPreCleaner in self.PRECLEANERS:
            sData = fPreCleaner(sData)

        return sData

    def write(self, sData):
        fOut = open(cardlist_folder(self._sFilename), "wb")
        # Normalise line ending
        sData = sData.replace(b'\r\n', b'\n')
        fOut.write(sData)
        fOut.close()


#
# pre-cleaners
#

def convert_to_utf8(sData):
    """Convert the data from some unknown encoding to utf8"""
    # Try utf8 first, then iso8859-1, which covers what we've
    # seen so far.
    aEncodings = ['utf8', 'iso8859-1']
    sRes = ''
    for sEnc in aEncodings:
        try:
            logging.info('Trying %s', sEnc)
            sRes = sData.decode(sEnc)
            break
        except UnicodeDecodeError:
            # We move onto the next one
            pass
    if sRes:
        return sRes.encode('utf8')


class VeknCardList(CardListSource):

    PRECLEANERS = [
            convert_to_utf8,
            ]

CARDLISTS = [
    VeknCardList("VEKN Cardlist", "cardlist.txt",
        "http://www.vekn.net/images/stories/downloads/cardlist.txt"),
    ]


def main():
    if not os.path.isdir(CACHE_FOLDER):
        os.mkdir(CACHE_FOLDER)

    aIndexes = []
    for oCardList in CARDLISTS:
        sData = oCardList.fetch()
        sData = oCardList.clean(sData)
        oCardList.write(sData)
        aIndexes.append(oCardList.index_entry())

    fIndex = open(cardlist_folder("index.txt"), "w")
    for sTitle, sFilename in aIndexes:
        fIndex.write("%s:%s\n" % (sTitle, sFilename))
    fIndex.close()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
