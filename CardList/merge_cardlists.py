#!/usr/bin/python
#
# This merges 2 card lists, taking the entry from the second one when
# there are duplicates.
#
# It is intended to work around cases where the vekn cardlist has not been
# updated, but a partial updated cardlist has been published by Black Chantry
# or some other source.

import sys
import unicodedata

aOld = []
aNew = []


def sort_key(sKey):
    """Sort of approximate cardlist sorting, to simplify diffs"""
    sNewKey = sKey.lower()
    sNewKey = unicodedata.normalize('NFKD', sNewKey).encode('ascii', 'ignore')
    if sNewKey.startswith('the '):
        sNewKey = sNewKey.replace('the ', '', 1) + '  the'
    # Sorting of puncation is odd in the cardlist - " is ignored, ' is
    # sorted after letters, and , is sorted before letters.
    # We fake it here
    sNewKey = sNewKey.replace("'", '_').replace(',', ' ').replace('"', '')
    return sNewKey


def process_file(sFilename):
    """Split the cardlist into a list of card entries"""
    dCardList = {}
    with open(sFilename, 'r') as fData:
        aCand = []
        sKey = u''
        for sLine in fData.readlines():
            sLine = sLine.strip()
            if sLine:
                sLine = sLine.decode('utf8')
                if u'Name: ' in sLine:
                    sKey = sLine.split(u':', 1)[1].strip()
                if u'Level: Advanced' in sLine:
                    sKey = sKey + u' (Advanced)'
                aCand.append(sLine)
            elif sKey:
                # End of card
                dCardList[sKey] = '\n'.join(aCand)
                aCand = []
                sKey = u''
            # Else we have a blank entry (double blank lines, etc),
            # so we just skip
        if sKey:
            # Dump last entry
            dCardList[sKey] = '\n'.join(aCand)
    return dCardList


def main(sOld, sNew):
    dOld = process_file(sOld)
    dNew = process_file(sNew)

    dCombined = dOld.copy()
    for sKey in dNew:
        dCombined[sKey] = dNew[sKey]

    aCardList = []
    for sKey in sorted(dCombined, key=sort_key):
        aCardList.append(dCombined[sKey])
    with open('merged_cardlist.txt', 'w') as fOut:
        fOut.write('\n\n'.join(aCardList).encode('utf8'))
        # Ensure we have a trailing newline
        fOut.write('\n')


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])





