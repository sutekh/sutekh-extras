#!/usr/bin/python3
# Extract some statistics about underused cards in the TWDA

import sys
import optparse
import csv
try:
    from sutekh.base.core.BaseTables import AbstractCard
    from sutekh.base.core.BaseAdapters import IKeyword
    from sutekh.SutekhUtility import is_crypt_card
    from twd_utils import start_twd_db, select_years, card_sets_with_progress
except ImportError as e:
    print e
    print 'Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?'
    sys.exit(1)


def parse_options(aArgs):
    """Handle option parsing"""
    sHelpText = "usage: %prog [year] [year2] ... [options]\n\n" + \
        "Finds stats about underused cards in the TWDA. "
    oOptParser = optparse.OptionParser(usage=sHelpText,
                                       version="%prog 0.1")
    oOptParser.add_option("-n", "--number", dest="number", default=2,
                          help="Number of cards to take as indicating"
                               " 'underused'.")
    aFormats = ["text", "csv", "tsv"]
    oOptParser.add_option("-f", "--format", dest="format", default='text',
                          choices=aFormats,
                          help="Format for output [%s]" % "|".join(aFormats))
    (oOpts, aRemaining) = oOptParser.parse_args(aArgs)
    if len(aRemaining) == 1:
        aYears = None
    else:
        aYears = aRemaining[1:]
    return oOpts, aYears


def find_underused_cards(aParCS, iNum):
    dStats = {}
    dAllTimeStats = {}
    oIllegal = IKeyword('not for legal play')
    for oCS in card_sets_with_progress(79):
        aCards = set([x.abstractCard for x in oCS.cards])
        for oCard in aCards:
            if oIllegal in oCard.keywords:
                # skip illegal cards
                continue
            sName = oCard.name
            if sName not in dAllTimeStats:
                if is_crypt_card(oCard):
                    sType = 'Crypt'
                else:
                    sType = 'Library'
                dAllTimeStats[sName] = [0, sType]
            dAllTimeStats[sName][0] += 1
            if oCS.parentID not in aParCS:
                # Only update dStats for the card sets of interest
                continue
            if sName not in dStats:
                dStats[sName] = 0
            dStats[sName] += 1
    aCrypt = []
    aLib = []
    iTotal = AbstractCard.select().count()
    sys.stderr.write('\n')
    sys.stderr.flush()
    iSeen = 0
    iFrac = 0
    iOldFrac = 0
    for oCard in AbstractCard.select():
        iSeen += 1
        iFrac = (iSeen * 79 + 2) / iTotal
        if iFrac > iOldFrac:
            sys.stderr.write('.')
            sys.stderr.flush()
        iOldFrac = iFrac
        if oIllegal in oCard.keywords:
            # skip illegal cards
            continue
        sName = oCard.name
        if sName not in dAllTimeStats:
            if is_crypt_card(oCard):
                aCrypt.append((sName, 0, 0))
            else:
                aLib.append((sName, 0, 0))
        elif sName not in dStats:
            iTotalCnt, sType = dAllTimeStats[sName]
            if sType == 'Crypt':
                aCrypt.append((sName, 0, iTotalCnt))
            else:
                aLib.append((sName, 0, iTotalCnt))
        elif dStats[sName] <= iNum:
            iTotalCnt, sType = dAllTimeStats[sName]
            if sType == 'Crypt':
                aCrypt.append((sName, dStats[sName], iTotalCnt))
            else:
                aLib.append((sName, dStats[sName], iTotalCnt))
    sys.stderr.write('\n')
    sys.stderr.flush()
    return aCrypt, aLib


def print_stats(aList, sType):
    print 'Underused %s cards' % sType
    aList.sort(key=lambda x: (-x[1], -x[2], x[0]))
    for sName, iNum, iTot in aList:
        print '%s (%d decks) (%d all-time)' % (sName.encode('utf8'),
                                               iNum, iTot)


def print_stats_tsv(aList, sType, iLow, iHigh):
    if iLow == iHigh:
        sRange = 'Occurances in %s' % iLow
    else:
        sRange = 'Occurances in %s-%s' % (iLow, iHigh)
    print "Underused %s cards" % sType
    print "Name\t%s\tAll-time occurances" % sRange
    aList.sort(key=lambda x: (-x[1], -x[2], x[0]))
    for sName, iNum, iTot in aList:
        print '%s\t%d\t%d' % (sName.encode('utf8'), iNum, iTot)


def print_stats_csv(aList, sType, iLow, iHigh):
    if iLow == iHigh:
        sRange = 'Occurances in %s' % iLow
    else:
        sRange = 'Occurances in %s-%s' % (iLow, iHigh)
    aList.sort(key=lambda x: (-x[1], -x[2], x[0]))
    writer = csv.writer(sys.stdout)
    writer.writerow(["Name", sRange, "All-time occurances"])
    for sName, iNum, iTot in aList:
        writer.writerow([sName.encode('utf8'), iNum, iTot])


if __name__ == "__main__":
    if not start_twd_db():
        print 'Environment not initialised'
        sys.exit(1)
    oOpts, aYears = parse_options(sys.argv)
    aParCS, iLowYear, iHighYear = select_years(aYears)
    if not aParCS:
        print 'Empty year card set list'
        sys.exit(1)
    crypt, library = find_underused_cards(aParCS, int(oOpts.number))
    if crypt is None or library is None:
        sys.exit(1)
    if oOpts.format == 'text':
        print_stats(crypt, 'Crypt')
        print
        print_stats(library, 'Library')
    elif oOpts.format == 'tsv':
        print_stats_tsv(crypt, 'Crypt', iLowYear, iHighYear)
        print
        print_stats_tsv(library, 'Library', iLowYear, iHighYear)
    elif oOpts.format == 'csv':
        print_stats_csv(crypt, 'Crypt', iLowYear, iHighYear)
        print
        print_stats_csv(library, 'Library', iLowYear, iHighYear)
