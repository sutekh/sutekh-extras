#!/usr/bin/python3
# Extract info on total card usage from the TWDA

from __future__ import print_function

import sys
import optparse
import csv
try:
    from sutekh.base.core.FilterParser import FilterParser
    from twd_utils import start_twd_db, select_years
    from stats_utils import (find_card_stats, print_stats, print_stats_csv,
                             print_stats_tsv)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


def parse_options(aArgs):
    """Handle option parsing"""
    sHelpText = "usage: %prog [year] [year2] ... [options]\n\n" + \
                "Finds stats about underused cards in the TWDA. "
    oOptParser = optparse.OptionParser(usage=sHelpText,
                                       version="%prog 0.1")
    aFormats = ["text", "csv", "tsv"]
    oOptParser.add_option("-f", "--format", dest="format", default='text',
                          choices=aFormats,
                          help="Format for output [%s]" % "|".join(aFormats))
    oOptParser.add_option("-l", "--filter", dest="filter", default=None,
                          help="Specify a filter to apply to the cards "
                          "checked, to allow restricting to a specific "
                          "expansion or so forth")
    (oOpts, aRemaining) = oOptParser.parse_args(aArgs)
    if len(aRemaining) == 1:
        aYears = None
    else:
        aYears = aRemaining[1:]
    return oOpts, aYears


if __name__ == "__main__":
    if not start_twd_db():
        print('Environment not initialised')
        sys.exit(1)
    oOpts, aYears = parse_options(sys.argv)
    aParCS, iLowYear, iHighYear = select_years(aYears)
    if not aParCS:
        print('Empty year card set list')
        sys.exit(1)
    if oOpts.filter:
        oParser = FilterParser()
        oFilter = oParser.apply(oOpts.filter).get_filter()
    else:
        oFilter = None
    crypt, library, total = find_card_stats(aParCS, oFilter)
    if crypt is None or library is None:
        sys.exit(1)
    if oOpts.format == 'text':
        print_stats(crypt, 'Crypt', iLowYear, iHighYear, total)
        print()
        print_stats(library, 'Library', iLowYear, iHighYear, total)
    elif oOpts.format == 'tsv':
        print_stats_tsv(crypt, 'Crypt', iLowYear, iHighYear, total)
        print()
        print_stats_tsv(library, 'Library', iLowYear, iHighYear, total)
    elif oOpts.format == 'csv':
        print_stats_csv(crypt, 'Crypt', iLowYear, iHighYear, total)
        print()
        print_stats_csv(library, 'Library', iLowYear, iHighYear, total)
