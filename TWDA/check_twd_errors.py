#!/usr/bin/python3
# Check for errors in the annotations of the TWD database
import sys
import datetime

try:
    import sutekh
    from twd_utils import (start_twd_db, sTWDURLIndicator, sTWDDateIndicator,
                           sTranslateString, sTWDParPrefix, sTWDRemoved,
                           sTWDUpdated, sOldTWDURLIndicator,
                           oLasombraTWDRetired, sLasombraTWDUrl,
                           sDefunctFranceTWDUrl, sSecretLibraryIndicator,
                           sSecretLibraryURLBase,
                           sDuplicateSLIndicator, sTWDAHolderParent)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

# Special cases

# These are known to have no month or an incorrect month in their title
aKnownBadMonth = [
    # Month is the start of the con, not the tournament date
    # Month in title is end of con, not tournament
    "Steve Coombs' DragonCon 2000: Atlanta, Georgia July 2000",
    # Misc other errors (usually month in title is month of
    # report, not tournament)
    u"Todd Banister's Columbia August 1998: Columbia,"
        " South Carolina August 1998",
    ]

# These are dated before The Lasombra retired from TWDA maintance, but were
# only added to the vekn twd.htm

aKnownMissedLasombra = [
    "Bram van Stappen's Dutch NC 2013: Utrecht, Netherlands "
        "September 2013",
    "Jordi Bestard's Nou Magaji a Ciutat: Palma de Mallorca, Spain"
        " September 2013",
    "Pierre Tran-Van's No Mercy for the Weak: Paris, France October 2013",
    "Goran Damjanic's Praxis Seizure: Pazin, Croatia October 2013",
    "Christofer Lindholm's Skin Trap: Stockholm, Sweden August 2013",
    u"Fernando Cesar's Brazilian NC 2009: S\xe3o Paulo, Brazil"
        " September 2009",
    "Michal Hrebejk's Czech NC 2011: Jihlava, Czech Republic August 2011",
    "Christophe Baltazar's French NC 2003 Day 1 - Last Chance Qualifier:"
        " Paris, France November 2003",
    "Laurent Pagorek's French NC 2003 Day 2: Paris, France November 2003",
    "Pierre Tran-Van's French NC 2009: Caen, France September 2009",
    "Pierre Tran-Van's French NC 2010: Lyon, France August 2010",
    "Martin Weinmayer's EC 1999 Day 2: Vienna, Austria November 1999",
    "Peter Rophail's Australian Championship 2005: Canberra, Australia"
        " October 2005",
    u"P\xe9ter Botos' Hungarian NC 2012: Debrecen, Hungary March 2012",
    u"Martin Miller's Pulled Fang #2 - Czech NC 2013: Jihlava, Czech"
        " Republic August 2013",
    u"Marius Iscru's ECQ - Geneva's purge part 2: Geneva, Switzerland"
        " September 2010",
    u"Marius Iscru's Grooming the Prot\xe9g\xe9: Strasbourg, France December"
        " 2006",
    u"Serge Cirri's Chill of Oblivion: Marseille, France April 2004",
    "César Ruipérez's Spanish NCQ: Valencia, Spain April 2006",
    "César Ruipérez's Vaulderie en Ayora, Spain December 2009",
    "César Ruipérez's Grand Slam de Andalucía, Spain August 2010",
    "César Ruipérez's Spanish ECQ - Sympathy for the Bleeding: San Vicente del Raspeig, Spain September 2012",
    "Bram Van Stappen's Dutch NC 2013: Utrecht, Netherlands September 2013",
    "Fernando Cesar's Brazilian NC 2009: São Paulo, Brazil September 2009",
    "Michal Hrebejk's Czech NC 2011: Jihlava, Czech Republic August 2011",

    ]

if not start_twd_db():
    sys.exit(1)  # Failure

dStats = {}
dSLUrls = {}
dTWDUrls = {}
iTotal = 0

for oCS in sutekh.PhysicalCardSet.select():
    if oCS.parent is None or oCS.parent.name == sTWDAHolderParent:
        # Skip TWD holders
        if oCS.name == sTWDAHolderParent:
            # skip top level holder entirely
            continue
        elif not oCS.name.startswith(sTWDParPrefix) and not len(oCS.name) == 8:
            if oCS.name != sTWDRemoved:
                print('Unexpected top level card set: %s' % oCS.name)
            if not oCS.annotations or sTWDUpdated not in oCS.annotations:
                print("No '%s' indicator in top level card set: %s" % (
                      sTWDUpdated, oCS.name))
        continue
    elif oCS.parent and not oCS.parent.name.startswith(sTWDParPrefix):
        if oCS.parent.name != sTWDRemoved:
            print('Unexpected Parent card set', oCS.parent.name)
    if oCS.parent.name not in dStats:
        dStats[oCS.parent.name] = 0
    if oCS.parent.name != sTWDRemoved:
        iTotal += 1
    dStats[oCS.parent.name] += 1
    if not oCS.inuse:
        print('Card Set %s not marked in use' % oCS.name)
    if not oCS.author:
        print('Missing author information for %s' % oCS.name)
    if not oCS.annotations:
        print('No annotations for %s' % oCS.name)
        continue
    if oCS.annotations and sTranslateString.lower() in oCS.annotations.lower():
        print('Translate marker in annotations, not description: %s' % oCS.name)
        continue
    if (oCS.annotations and
            sTranslateString.lower() in oCS.annotations.lower() and
            sTranslateString not in oCS.annotations):
        print('Translate marker in annotations with wrong case: %s' % oCS.name)
        continue
    # Have data, so continue
    if oCS.parent.name == sTWDRemoved:
        # None of these checks matter for removed card sets
        continue
    if sTWDURLIndicator not in oCS.annotations:
        print('Missing TWD URL in %s' % oCS.name)
        # Don't skip date checks just yet
    if sTWDUpdated in oCS.annotations:
        print("Unexpected '%s' indicator in %s" % (sTWDUpdated, oCS.name))
    if sTWDDateIndicator not in oCS.annotations and \
            sTWDDateIndicator.lower() in oCS.annotations.lower():
        print('Wrong TWD Date marker in %s' % oCS.name)
        continue
    elif sTWDDateIndicator not in oCS.annotations:
        print('Missing TWD Date in %s' % oCS.name)
        continue
    else:
        if sTWDURLIndicator in oCS.annotations:
            if (oCS.annotations.find(sTWDDateIndicator) <
                    oCS.annotations.find(sTWDURLIndicator)):
                print('TWD Date before TWD URL in %s' % oCS.name)
    # Have a date, so check for consistency issues
    sPrevLine = None
    iUrlLine = -1
    iOldUrlLine = 1000
    for iLineNo, sLine in enumerate(oCS.annotations.split('\n')):
        if sPrevLine is None:
            if not sLine.strip():
                print('Leading blank line in %s' % oCS.name)
                break
        elif not sLine.strip() and not sPrevLine.strip():
            print('Double blank lines in %s' % oCS.name)
            break
        if sLine.startswith(sTWDURLIndicator):
            if sLasombraTWDUrl in sLine:
                print('TWD URL still points to lasombra.com in %s' % oCS.name)
            elif sDefunctFranceTWDUrl in sLine:
                print('TWD URL still points to veknfrance.com in %s' % oCS.name)
            iUrlLine = iLineNo
            sTWDUrl = sLine.split(':', 1)[1].strip()
            dTWDUrls.setdefault(sTWDUrl, [])
            dTWDUrls[sTWDUrl].append(oCS.name)
        if sLine.startswith(sOldTWDURLIndicator):
            iOldUrlLine = iLineNo
        if (sLine.startswith(sSecretLibraryIndicator) or
                sLine.startswith(sDuplicateSLIndicator)):
            sSLUrl = sLine.split(':', 1)[1].strip()
            dSLUrls.setdefault(sSLUrl, [])
            dSLUrls[sSLUrl].append(oCS.name)
        if sSecretLibraryIndicator in sLine and not (
                sLine.startswith(sSecretLibraryIndicator) or
                sLine.startswith(sDuplicateSLIndicator)):
            print("SL Indicator isn't at the start of a line in %s" % oCS.name)
        if sSecretLibraryIndicator in sLine and iLineNo > iOldUrlLine:
            print('SL Indicator after Old TWDA indicator in %s' % oCS.name)
        if sLine.startswith(sTWDDateIndicator):
            if iLineNo > (iUrlLine + 1) and sPrevLine is not None:
                if sPrevLine.strip():
                    print('Text between TWD Url marker and Date marker '
                          'in %s: %s' % (oCS.name, sPrevLine))
                else:
                    print('Blank line between TWD Url and TWD Date markers '
                          ' in %s' % oCS.name)
            sDate = sLine.split(':', 1)[1].strip()
            if ' # ' in sDate:
                sDate = sDate.split('#', 1)[0].strip()
            if sDate.startswith('200') or sDate.startswith('199') or \
                    sDate.startswith('201') or sDate.startswith('202'):
                oDate = datetime.datetime.strptime(sDate, '%Y-%m-%d')
                sMonth = oDate.strftime('%b')
                sYear = oDate.strftime('%Y')
                if sMonth not in oCS.name:
                    if oCS.name not in aKnownBadMonth:
                        print('Possibly wrong month in %s'
                              ' (date %s, month %s)' % (oCS.name,
                                                        sDate,
                                                        sMonth))
                if sYear not in oCS.name or sYear not in oCS.parent.name:
                    print('Possibly wrong year in %s (date %s, year %s)' 
                          % (oCS.name, sDate, sYear))
            else:
                # Possibly wrong date format
                try:
                    oDate = datetime.datetime.strptime(sDate, '%d-%m-%Y')
                    sFixedDate = oDate.strftime('%Y-%m-%d')
                    sLine = '%s %s' % (sTWDDateIndicator, sFixedDate)
                    print('Wrong date format for %s' % oCS.name)
                    print('Got : %s Expected : %s' % (sDate, sFixedDate))
                except ValueError:
                    print('Unknown date format in %s (date %s)' % (oCS.name,
                                                                   sDate))
        sPrevLine = sLine

print
for sCSName in sorted(dStats):
    print('%10s : %d ' % (sCSName, dStats[sCSName]))
print()
print('Total decks (excluding removed decks): %d' % iTotal)

for sSLUrl, aNames in dSLUrls.items():
    if len(aNames) > 1:
        print('Duplicate entries for Secret Library url %s' % sSLUrl)
        print('\n    '.join(aNames))
    if not sSLUrl.startswith(sSecretLibraryURLBase):
        print('Invalid Secret Library url %s' % sSLUrl)
        print('\n    '.join(aNames))

for sTWDUrl, aNames in dTWDUrls.items():
    if len(aNames) > 1:
        print('Duplicate entries for TWD url %s' % sTWDUrl)
        print('\n    '.join(aNames))
