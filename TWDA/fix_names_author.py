#!/usr/bin/python3

import sys

sDeckNames = "/home/neil/amusements/Sutekh/TWD/deck_names.htm"


def extract_info(sName, aLines):
    bFound = False
    with open(sDeckNames, 'r') as oDeckNames:
        for sDeckLine in oDeckNames:
            sFullUrlHRef = ('href="http://www.vekn.fr/decks/twd.htm#%s"'
                            % sName.lower())
            for sHRef in ['href=#%s>' % sName.lower(),
                          'href="#%s"' % sName.lower()]:
                if sHRef in sDeckLine.lower() or sFullUrlHRef in sDeckLine.lower():
                    bFound = True
                    sDeckName = sDeckLine.split('>', 1)[1]
                    sDeckName = sDeckName.split('<', 1)[0]
                    break

    if not bFound:
        return False
    # Find author line
    aLines.append('Deck Name: %s' % sDeckName)
    aLines.append('Author:')
    aLines.append('Description:')
    with open(sName, 'r') as oDeck:
        for sLine in oDeck:
            # Drop noise from html splitting
            if sLine.startswith('<a name="%s"' % sName) or sLine.startswith('<a name=%s' % sName):
                continue
            if sLine.startswith('<a id="%s"' % sName) or  sLine.startswith('<a id=%s' % sName):
                continue
            if not sLine.replace('-', '').strip():
                continue
            if sLine.startswith('</pre><a href='):
                continue
            if sLine.startswith('<pre><a href='):
                continue
            if sLine.startswith('<hr><pre>'):
                continue
            if sLine.startswith('</pre><br>'):
                continue
            if sLine.startswith('<a href="#top"'):
                continue
            if sLine.startswith('<a href="http://www.veknfrance.com/'):
                continue
            if sLine.startswith('<a href="http://www.vekn.fr/'):
                continue
            # Valid line, so keep it
            sLine = sLine.strip()
            aLines.append(sLine)
            sTest = sLine.lower()
            if sTest.startswith('author:') or sTest.startswith('author :') or \
                    sTest.startswith('created by:') or \
                    sTest.startswith('created by :') or \
                    sTest.startswith('deck author:') or \
                    sTest.startswith('deck author :'):
                sAuthor = sLine.split(':', 1)[1].strip()
                aLines[1] = "Author: %s" % sAuthor
    return True


def write_file(sName, aLines):
    with open(sName, 'w') as oDeck:
        oDeck.write('\n'.join(aLines))

if __name__ == "__main__":
    for sFileName in sys.argv[1:]:
        aLines = []
        if extract_info(sFileName, aLines):
            write_file(sFileName, aLines)
        else:
            print(f"Unable to fix {sFileName}")
