#!/usr/bin/python3
# Check for errors in the annotations of the TWD database
import sys

try:
    import sutekh
    from twd_utils import start_twd_db, sTWDURLIndicator, sTWDAHolderParent
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

if not start_twd_db():
    sys.exit(1)  # Failure

try:
    names = sys.argv[1:]
except IndexError:
    print('Usage %s <split deck name> [<split deck name>] [...]' % sys.argv[0])

if names[0] == '*':
    print('Directory appears empty')
    sys.exit(1)

for oCS in sutekh.PhysicalCardSet.select():
    # Inefficent, but will work
    if oCS.parent is None or oCS.parent.name == sTWDAHolderParent:
        continue
    if sTWDURLIndicator in oCS.annotations:
        for sLine in oCS.annotations.split('\n'):
            for name in names:
                if sLine.startswith(sTWDURLIndicator) and sLine.endswith(name):
                    print('%s appears to be in the database as %s' % (
                        name, oCS.name.encode('utf8')))
