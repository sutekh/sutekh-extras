#!/usr/bin/python3
# Find any urls that are in the database, but no longer in the twd

import sys

try:
    import sutekh
    from twd_utils import (sTWDURLIndicator, start_twd_db, parse_fixer_args, sTWDRemoved,
                           sVEKNFranceTWDUrl, get_twd_file, card_sets_with_progress)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

if not start_twd_db():
    sys.exit(1)

oOpts = parse_fixer_args(sys.argv)

oTWDFile = get_twd_file()
if not oTWDFile:
    sys.exit(1)  # Failed to open file

aTWDFileUrls = []

for sLine in oTWDFile:
    sLineLow = sLine.lower()
    if ('a href=#' not in sLineLow and
            'a href="http://www.vekn.fr/' not in sLineLow):
        continue
    if '<td colspan="2"><a href="#year' in sLineLow:
        continue
    if 'twd.htm#year' in sLineLow:
        continue
    if '<td><a href="#year' in sLineLow:
        continue
    if '<a href="#top">' in sLineLow:
        continue
    if 'ref=#>top</a>' in sLineLow:
        continue
    if 'href="#">top</a>' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks/twd.htm#top">' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks.htm">back</a>' in sLineLow:
        continue
    if '<a href="http://www.thelasombra.com/hall_of_fame.htm">' in sLineLow:
        continue
    if '<a href="http://www.vekn.fr/decks.htm">back</a>' in sLineLow:
        continue
    # TWD Entry
    sName = sLine.split('>')[1]
    sName = sName.split('<')[0].strip()
    sKey = sLine.split('=')[1].split('>')[0]
    if '#' in sKey:
        sKey = sKey.split('#')[1]
        sKey = '#' + sKey
    sUrl = sVEKNFranceTWDUrl + sKey
    aTWDFileUrls.append(sUrl)

aRemoved = []

for oCS in card_sets_with_progress():
    if sTWDURLIndicator not in oCS.annotations:
        continue
    if oCS.parent.name == sTWDRemoved:
        continue
    for sLine in oCS.annotations.splitlines():
        if sLine.startswith(sTWDURLIndicator):
            sUrl = sLine.replace(sTWDURLIndicator, '').strip()
            if sUrl not in aTWDFileUrls:
                aRemoved.append((oCS.name, sUrl))

print(aRemoved)
