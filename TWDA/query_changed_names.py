import sys

try:
    import sutekh
    from sutekh.base.Utility import to_ascii
    from twd_utils import (start_twd_db, sTWDURLIndicator, sVEKNFranceTWDUrl,
                           get_twd_file, sTWDParPrefix, check_exact_match)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


sBaseUrl = '%s %s' % (sTWDURLIndicator, sVEKNFranceTWDUrl)

if '--help' in sys.argv:
    print('Usage: %s [--skip-unicode] [--diff]' % sys.argv[0])
    print('List deck names that differ between the database'
          ' and the twd.htm list')
    print('With --skip-unicode , ignore unicode only differences')
    print('With --diff , use difflib to show the differences')
    sys.exit(0)

if not start_twd_db():
    sys.exit(1)

oTWDFile = get_twd_file()
if not oTWDFile:
    sys.exit(1)  # Failed to open file

import difflib

oDiff = difflib.Differ()

dStats = {}

for sLine in oTWDFile:
    sLineLow = sLine.lower()
    if 'a href=#' not in sLineLow:
        continue
    if '<td><a href="#year' in sLineLow:
        continue
    if '<a href="#top">' in sLineLow:
        continue
    if '<a href="#">' in sLineLow:
        continue
    if 'href=#>top</a>' in sLineLow:
        continue
    if '<td colspan="2"><a href="#year' in sLineLow:
        continue
    if 'twd.htm#year' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks/twd.htm#top">' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks.htm">back</a>' in sLineLow:
        continue
    if '<a href="http://www.thelasombra.com/hall_of_fame.htm">' in sLineLow:
        continue
    if '<a href="http://www.vekn.fr/decks.htm">back</a>' in sLineLow:
        continue
    # TWD Entry
    sMatchReason = 'unknown means'
    sName = sLine.split('>')[1]
    sName = sName.split('<')[0].strip()
    sKey = sLine.split('=')[1].split('>')[0]
    sUrl = sBaseUrl + sKey
    # The new vekn list uses &amp; everywhere
    if '&amp;' in sName:
        sName = sName.replace('&amp;', '&')
    try:
        oCS = sutekh.PhysicalCardSet.byName(sName)
    except Exception as e:
        sFiltName = sName.encode('ascii',
                                 'replace').replace(b'?', b'_').decode('utf8')
        oFilter = sutekh.CardSetNameFilter(sFiltName)
        aCS = list(oFilter.select(sutekh.PhysicalCardSet))
        if len(aCS) != 1:
            oFilter = sutekh.CardSetAnnotationsFilter(sUrl)
            aPossUrlCS = list(oFilter.select(sutekh.PhysicalCardSet))
            aUrlCS = []
            for oCS in aPossUrlCS:
                if check_exact_match(oCS, sUrl):
                    aUrlCS.append(oCS)
            if len(aUrlCS) != 1:
                print("======================")
                print(sLine, "doesn't match a unique card set")
                print('Wildcard name', sFiltName)
                if aCS:
                    print('Name candidates are', aCS)
                if aUrlCS:
                    print('TWD Url Candidates are', aUrlCS)
                if not aCS and not aUrlCS:
                    print('No candidates')
                print(sUrl)
                print("=======================")
                continue
            else:
                oCS = aUrlCS[0]
                sMatchReason = 'TWD url'
        else:
            oCS = aCS[0]
            sMatchReason = 'wildcard Name match'

    if sName != oCS.name:
        sMsg = None
        oRes = list(oDiff.compare([sName], [oCS.name]))
        if to_ascii(sName) != to_ascii(oCS.name):
            sMsg = "Matching (TWD) %s with %s by %s" % (sName, oCS.name,
                                                        sMatchReason)
        else:
            sMsg = "Unicode differences between TWD and database:"
            sMsg += " Matching (TWD) %s (%s) with %s (%s) by %s" % (
                sName, to_ascii(sName), oCS.name, to_ascii(oCS.name),
                sMatchReason)
        if sMsg:
            print(sMsg.encode('ascii', 'replace').decode('ascii'))
            print('Diff:')
            print('\n'.join(oRes).encode('ascii', 'replace').decode('ascii'))
            sResp = input('Fix name (y / n)')
            if sResp.lower() == 'y':
                oCS.name = sName
                oCS.syncUpdate()

for sKey in sorted(dStats):
    print('%s  : %d' % (sKey.replace(sTWDParPrefix, ''), dStats[sKey]))
