#!/usr/bin/python3
# Create zip files for the TWD database

import sys
import os
import datetime
from logging import Handler
try:
    import sutekh
    from twd_utils import start_twd_db, sTWDUpdated, sTWDAHolderParent
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


if not start_twd_db():
    sys.exit(1)

sDateUpdated = datetime.date.today().strftime('%Y-%m-%d')


class DotHandler(Handler, object):
    """Emit a dot for each card set, emitting the year seperately"""

    def __init__(self, sStartString):
        self.sStartString = sStartString
        super(DotHandler, self).__init__()

    def emit(self, oRecord):
        sMsg = self.format(oRecord)
        if sMsg.startswith('PCS: Tournament Winning Decks'):
            # Avoid leading '.' on output for cosemtic reasons
            return
        if sMsg.startswith(self.sStartString):
            sys.stdout.write('Writing %s\n' %
                             sMsg.replace('PCS: ',
                                          '').replace(' written', ''))
            sys.stdout.flush()
        else:
            sys.stdout.write('.')
            sys.stdout.flush()

oLogHandler = DotHandler('PCS: TWD')

# We select all card sets with no parent
aTopLevelCardSets = [x for x in sutekh.PhysicalCardSet.select() if
                     (x.parent is None or x.parent.name == sTWDAHolderParent)]

oHolder = sutekh.PhysicalCardSet.byName(sTWDAHolderParent)

bDoFull = False


def make_zip_file(sZipName, aZipCS):
    oZipFile = sutekh.io.ZipFileWrapper.ZipFileWrapper(sZipName)
    if os.path.exists(sZipName):
        aExistingList = list(oZipFile.get_all_entries())
        aZipNames = [oCS.name for oCS in aZipCS]
        if sorted(aZipNames) == sorted(aExistingList):
            print('Skipping %s as zip contents identical' % sZipName)
            return False
    if not oParCS.annotations:
        oParCS.annotations = '%s %s' % (sTWDUpdated, sDateUpdated)
        oParCS.syncUpdate()
    elif sTWDUpdated not in oParCS.annotations:
        oParCS.annotations += '\n%s %s' % (sTWDUpdated, sDateUpdated)
        oParCS.syncUpdate()
    else:
        try:
            sExistingDate = oParCS.annotations.split(sTWDUpdated)[1][1:11]
            # Sanity check
            _oDate = datetime.datetime.strptime(sExistingDate, '%Y-%m-%d')
            oParCS.annotations = oParCS.annotations.replace(sExistingDate,
                                                            sDateUpdated)
            oParCS.syncUpdate()
        except Exception as oErr:
            print("Unable to correctly update the date for %s" % oParCS.name)
            print(oErr)
    oZipFile.do_dump_list_to_zip(aZipCS, oLogHandler)
    print()
    return True


for oParCS in aTopLevelCardSets:
    if oParCS.name == sTWDAHolderParent:
        continue
    oParFilter = sutekh.core.Filters.ParentCardSetFilter([oParCS.name])
    aChildren = oParFilter.select(sutekh.PhysicalCardSet)
    aChildren = list(aChildren)
    if oParCS.parent and oParCS.parent.name == sTWDAHolderParent:
        aZipCS = [oHolder, oParCS] + aChildren
    else:
        aZipCS = [oParCS] + aChildren
    # Skip holders with no entries yet
    if len(aChildren) == 0:
        continue
    sZipName = 'Sutekh_%s.zip' % oParCS.name.replace(' ', '_')
    if make_zip_file(sZipName, aZipCS):
        bDoFull = True

# Restrict non-dotting for all CS's
if not bDoFull:
    print('Skipping Sutekh_TWDA.zip as no new zip files written')
else:
    oLogHandler = DotHandler('zzzzz')
    print('Writing Sutekh_TWDA.zip')
    oZipFile = sutekh.io.ZipFileWrapper.ZipFileWrapper('Sutekh_TWDA.zip')
    oZipFile.do_dump_all_to_zip(oLogHandler)
    print()
