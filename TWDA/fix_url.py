#!/usr/bin/python3
# Append the url parameter to the card set annotations for the TWD
# atchives

from __future__ import print_function

import sys

try:
    import sutekh
    from twd_utils import (sTWDURLIndicator, start_twd_db, parse_fixer_args,
                           sVEKNFranceTWDUrl, get_twd_file, check_exact_match)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


sBaseUrl = '%s %s' % (sTWDURLIndicator, sVEKNFranceTWDUrl)

if not start_twd_db():
    sys.exit(1)

oOpts = parse_fixer_args(sys.argv)

oTWDFile = get_twd_file()
if not oTWDFile:
    sys.exit(1)  # Failed to open file

for sLine in oTWDFile:
    sLineLow = sLine.lower()
    if ('a href=#' not in sLineLow and
            'a href="http://www.vekn.fr/' not in sLineLow):
        continue
    if '<td colspan="2"><a href="#year' in sLineLow:
        continue
    if 'twd.htm#year' in sLineLow:
        continue
    if '<td><a href="#year' in sLineLow:
        continue
    if '<a href="#top">' in sLineLow:
        continue
    if 'ref=#>top</a>' in sLineLow:
        continue
    if 'href="#">top</a>' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks/twd.htm#top">' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks.htm">back</a>' in sLineLow:
        continue
    if '<a href="http://www.thelasombra.com/hall_of_fame.htm">' in sLineLow:
        continue
    if 'the original <a href="http://www.vekn.fr/hall_of_fame.htm">hall of fame<' in sLineLow:
        continue
    if '<a href="http://www.vekn.fr/decks.htm">back</a>' in sLineLow:
        continue
    # TWD Entry
    sName = sLine.split('>')[1]
    sName = sName.split('<')[0].strip()
    sKey = sLine.split('=')[1].split('>')[0]
    if '#' in sKey:
        sKey = sKey.split('#')[1]
        sKey = '#' + sKey
    sUrl = sBaseUrl + sKey
    try:
        oCS = sutekh.PhysicalCardSet.byName(sName)
    except Exception as e:
        sName = sName.encode('ascii', 'replace').replace(b'?', b'_').decode('ascii')
        oFilter = sutekh.CardSetNameFilter(sName)
        aCS = list(oFilter.select(sutekh.PhysicalCardSet))
        if len(aCS) != 1:
            oFilter = sutekh.CardSetAnnotationsFilter(sUrl)
            aPossCS = list(oFilter.select(sutekh.PhysicalCardSet))
            aCS = []
            for oCS in aPossCS:
                if check_exact_match(oCS, sUrl):
                    aCS.append(oCS)
            if len(aCS) != 1:
                print("%s doesn't match an unique card set" % sLine)
                print("Matched on TWD url: %s" % sUrl)
                if len(aCS) > 1:
                    print("Card Sets", aCS)
                else:
                    print("No matching card sets")
                print()
            elif oOpts.verbose:
                print('Matching %s with %s' % (sName,
                                               aCS[0].name))
            continue
        else:
            oCS = aCS[0]
    if oCS.annotations and sUrl in oCS.annotations:
        continue
    else:
        if oCS.annotations:
            if sTWDURLIndicator not in oCS.annotations:
                sNewAnnotations = oCS.annotations + '\n\n%s' % sUrl
            else:
                print('Inconsistent TWD url for %s' % oCS.name)
                print('Current annotations : %s ' % oCS.annotations)
                print('Calculated Url: %s' % sUrl)
                continue
        else:
            sNewAnnotations = sUrl
        sAction = 'Setting TWD Url for %s to %s' % (oCS.name,
                                                    sUrl)
        if oOpts.commit:
            print(sAction)
            oCS.annotations = sNewAnnotations
            oCS.syncUpdate()
        else:
            print('Calculated action: %s' % sAction)
