# stats_utils.py
# Utilities for displaying / outputing TWD stats info

import csv
import sys


from sutekh.base.core.BaseTables import (MapPhysicalCardToPhysicalCardSet,
                                         AbstractCard)
from sutekh.base.core.BaseAdapters import IAbstractCard
from sutekh.base.core.BaseFilters import (PhysicalCardSetFilter, FilterAndBox)
from sutekh.SutekhUtility import is_crypt_card

from twd_utils import card_sets_with_progress

LIB_BIN_BOUNDARIES = [1, 2, 3, 4, 5, 7, 11, 16, 21, 26, 1000]
CRYPT_BIN_BOUNDARIES = [1, 2, 3, 4, 5, 6, 7, 11, 21, 1000]

# We exclude the storyline cards, since they should never appear in the TWDA
STORYLINE_EXPANSIONS = [
    "Nergal Storyline",
    "Cultist Storyline",
    "Eden's Legacy Storyline",
    "March of the War Ghouls (NAC 2012)",
    "The Battle for Budapest (EC 2012)",
    "Ragnarok - the Final Battle (EC 2013)",
    "VEKN 2014 Children of Osiris",
    "VEKN 2014 The Returned",
    "VEKN 2014 Rise of the Nictuku",
    "VEKN 2015 The Red Sign",
    "Ascension of Caine (EC 2017)",
    ]

# Cards that were never legal for tournament play, and never will be
ALWAYS_ILLEGAL = [
    "Madness of the Bard",
    "Playing for Keeps",
    "High Stakes",
    "Cunctator Motion",
    ]


def check_card_illegal(oCard):
    """Check if the card should be excluded."""
    # Skip banned cards
    if oCard.name in ALWAYS_ILLEGAL:
        return True
    for sExp in [oP.expansion.name for oP in oCard.rarity]:
        if sExp in STORYLINE_EXPANSIONS:
            return True
    return False


def find_card_stats(aParCS, oFilter):
    dStats = {}
    iDeckTotal = 0
    for oCS in card_sets_with_progress(79):
        if oCS.parentID not in aParCS:
            # Only update dStats for the card sets of interest
            continue
        iDeckTotal += 1
        if oFilter:
            oBaseFilter = PhysicalCardSetFilter(oCS.name)
            oJointFilter = FilterAndBox([oBaseFilter, oFilter])
            aCards = [IAbstractCard(x) for x in
                      oJointFilter.select(MapPhysicalCardToPhysicalCardSet)]
        else:
            aCards = [x.abstractCard for x in oCS.cards]
        dDeck = {}
        for oCard in aCards:
            if check_card_illegal(oCard):
                continue
            dDeck.setdefault(oCard, 0)
            dDeck[oCard] += 1
        for oCard in dDeck:
            sName = oCard.name
            if is_crypt_card(oCard):
                aBins = CRYPT_BIN_BOUNDARIES
            else:
                aBins = LIB_BIN_BOUNDARIES
            if sName not in dStats:
                if is_crypt_card(oCard):
                    sType = 'Crypt'
                else:
                    sType = 'Library'
                dStats[sName] = [0, sType, [0] * len(aBins)]
            dStats[sName][0] += 1
            # find the correct bin
            iCount = dDeck[oCard]
            for iBin, iNum in enumerate(aBins):
                if iCount >= iNum and iCount < aBins[iBin + 1]:
                    dStats[sName][2][iBin] += 1
                    break
    aCrypt = []
    aLib = []
    sys.stderr.write('\n')
    sys.stderr.flush()
    iSeen = 0
    iFrac = 0
    iOldFrac = 0
    if oFilter:
        aCards = oFilter.select(AbstractCard)
        iTotal = oFilter.select(AbstractCard).count()
    else:
        aCards = AbstractCard.select()
        iTotal = AbstractCard.select().count()
    for oCard in aCards:
        iSeen += 1
        iFrac = (iSeen * 79 + 2) / iTotal
        if iFrac > iOldFrac:
            sys.stderr.write('.')
            sys.stderr.flush()
        iOldFrac = iFrac
        if check_card_illegal(oCard):
            continue
        sName = oCard.name
        if sName not in dStats:
            if is_crypt_card(oCard):
                aCrypt.append((sName, 0, [0] * (len(CRYPT_BIN_BOUNDARIES) - 1)))
            else:
                aLib.append((sName, 0, [0] * (len(LIB_BIN_BOUNDARIES) - 1)))
        else:
            aHist = dStats[sName][2][:-1]
            sType = dStats[sName][1]
            if sType == 'Crypt':
                aCrypt.append((sName, dStats[sName][0], aHist))
            else:
                aLib.append((sName, dStats[sName][0], aHist))
    sys.stderr.write('\n')
    sys.stderr.flush()
    return aCrypt, aLib, iDeckTotal


def make_bin_header(sType):
    aHeader = []
    if sType.lower() == 'crypt':
        aBins = CRYPT_BIN_BOUNDARIES
    else:
        aBins = LIB_BIN_BOUNDARIES
    for iBin, iNum in enumerate(aBins[:-1]):
        iUpper = aBins[iBin + 1] - 1
        if iUpper == iNum:
            aHeader.append('%d' % iNum)
        elif iUpper < 99:
            aHeader.append('%d - %d' % (iNum, iUpper))
        else:
            aHeader.append('%d - ' % iNum)
    return aHeader


def gen_stats_string(aList, sType, iLow, iHigh, iTotal):
    if iLow == iHigh:
        sRange = 'Occurances in %s' % iLow
    else:
        sRange = 'Occurances in %s-%s' % (iLow, iHigh)
    aOutput = ['Usage of %s cards' % sType]
    aOutput.append("Total decks: %d" % iTotal)
    if sType.lower() == 'library':
        aOutput.append('Ordering: Name, number of decks,'
                       ' (1 card, 2 cards, 3 cards, 4 cards, 5-6 cards,'
                       ' 7-10 cards, 11-15 cards, 16-20 cards,'
                       ' 21-25 cards, 25+ cards)')
    else:
        aOutput.append('Ordering: Name, number of decks,'
                       ' (1 card, 2 cards, 3 cards, 4 cards, 5 cards,'
                       ' 6 cards, 7-10 cards, 10-20 cards, 20+ cards)')
    aList.sort(key=lambda x: (-x[1], x[0]))
    for sName, iNum, aHist in aList:
        sHist = ', '.join(['%d' % x for x in aHist])
        aOutput.append('%s (%d decks) (%s)' % (sName,
                                               iNum, sHist))
    return '\n'.join(aOutput)


def gen_stats_for_csv_tsv(aList, sType, iLow, iHigh, iTotal):
    if iLow == iHigh:
        sRange = 'Occurances in %s' % iLow
    else:
        sRange = 'Occurances in %s-%s' % (iLow, iHigh)
    aHeader = ["Usage of %s cards" % sType]
    aHeader.append("Total decks: %d" % iTotal)
    aDataRows = []
    aSingleRow = ["Name", sRange]
    aSingleRow.extend(make_bin_header(sType))
    aDataRows.append(aSingleRow)
    aList.sort(key=lambda x: (-x[1], x[0]))
    for sName, iNum, aHist in aList:
        aSingleRow = [sName, iNum]
        aSingleRow.extend(aHist)
        aDataRows.append(aSingleRow)
    return aHeader, aDataRows


def print_stats(aList, sType, iLow, iHigh, iTotal):
    print(gen_stats_string(aList, sType, iLow, iHigh, iTotal))


def save_stats_to_file(aList, sType, iLow, iHigh, iTotal, sFileName):
    with open(sFileName, 'w') as fOut:
        fOut.write(gen_stats_string(aList, sType, iLow, iHigh, iTotal))


def output_csv_tsv(aList, sType, iLow, iHigh, iTotal, fOut, oDialect):
    aHeader, aDataRows = gen_stats_for_csv_tsv(aList, sType, iLow, iHigh, iTotal)
    fOut.write(oDialect.lineterminator.join(aHeader))
    fOut.write(oDialect.lineterminator)
    writer = csv.writer(fOut, dialect=oDialect)
    for aRow in aDataRows:
        writer.writerow(aRow)


def print_stats_tsv(aList, sType, iLow, iHigh, iTotal):
    output_csv_tsv(aList, sType, iLow, iHigh, iTotal, sys.stdout, csv.excel_tab)


def print_stats_csv(aList, sType, iLow, iHigh, iTotal):
    output_csv_tsv(aList, sType, iLow, iHigh, iTotal, sys.stdout, csv.excel)


def save_tsv_to_file(aList, sType, iLow, iHigh, iTotal, sFileName):
    with open(sFileName, 'w') as fOut:
        output_csv_tsv(aList, sType, iLow, iHigh, iTotal, fOut, csv.excel_tab)


def save_csv_to_file(aList, sType, iLow, iHigh, iTotal, sFileName):
    with open(sFileName, 'w') as fOut:
        output_csv_tsv(aList, sType, iLow, iHigh, iTotal, fOut, csv.excel)
