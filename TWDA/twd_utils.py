# twd_utils.py
# Helpful utilities for the twd db manipulation stuff

import os
import sys
import optparse
import datetime
import unicodedata
from sqlobject import SQLObjectNotFound

import sutekh
from sutekh.base.core.BaseAdapters import IPhysicalCardSet

# Useful constants
sOldTWDURLIndicator = 'Old Lasombra TWDA URL:'
sTWDURLIndicator = 'TWDA URL:'
sTWDDateIndicator = 'TWDA Date:'
sSecretLibraryIndicator = 'SL URL:'
sSecretLibraryURLBase = 'https://www.secretlibrary.info/'
sDuplicateSLIndicator = 'Duplicate SL URL:'
sTWDFile = "twd.htm"
sTWDUpdated = "Date Updated:"
# Prefix used for TWDA decks
sTWDParPrefix = 'TWDA '
# Holder for all TWDA decks
sTWDAHolderParent = 'Tournament Winning Decks'
# Holder for Removed card sets
sTWDRemoved = 'REMOVED'
sLasombraTWDUrl = 'http://www.thelasombra.com/decks/twd.htm'
sDefunctFranceTWDUrl = 'http://www.veknfrance.com/decks/twd.htm'
sVEKNFranceTWDUrl = 'http://www.vekn.fr/decks/twd.htm'
sTranslateString = 'Google translate'

# We set the retirement date to late in the day so comparisons work as expected
oLasombraTWDRetired = datetime.datetime(year=2013, month=10, day=26, hour=23)


def start_twd_db():
    """Start sutekh, enabling the twd db"""
    sTWDPath = os.getenv("SUTEKH_TWD_DIR", None)
    if not sTWDPath:
        print('SUTEKH_TWD_DIR no set, aborting')
        return False
    sTWDdb = 'sqlite:///%s/twd.db' % sTWDPath
    sutekh.start(['sutekh', '-d', sTWDdb])
    return True


def get_twd_file():
    sTWDPath = os.getenv("SUTEKH_TWD_DIR", None)
    if not sTWDPath:
        print('SUTEKH_TWD_DIR no set, aborting')
        return None
    return open(os.path.join(sTWDPath, sTWDFile))


def parse_fixer_args(aArgs):
    """Standardised argument parsing for the fix_x helpers"""
    oOptParser = optparse.OptionParser(usage="usage: %prog [options]",
                                       version="%%prog 1.0")
    oOptParser.add_option('-v', '--verbose', action="store_true",
                          dest="verbose", default=False,
                          help="More verbose output")
    oOptParser.add_option('-c', '--commit', action="store_true",
                          dest="commit", default=False,
                          help="Actually commit changes to the database")
    oOpts, aArgsLeft = oOptParser.parse_args(aArgs)
    if len(aArgsLeft) != 1:
        oOptParser.print_help()
        sys.exit(1)
    return oOpts


def get_twd_path():
    return os.getenv("SUTEKH_TWD_DIR", None)


def select_years(aYears):
    """Returns a list of card set ids corresponding to the given years,
       along with the range.  Return an empty list if the input is invalid.

       Helper function for stat extracting tools"""
    aCS = []
    iLowYear = 9999
    iHighYear = 0
    if not aYears:
        # Treat this case as selecting everything
        for oCS in sutekh.PhysicalCardSet.select():
            if oCS.parent and oCS.parent.name == sTWDAHolderParent:
                aCS.append(oCS.id)
                iYear = int(str(oCS.name).replace(sTWDParPrefix, ''))
                if iYear > iHighYear:
                    iHighYear = iYear
                if iYear < iLowYear:
                    iLowYear = iYear
    else:
        for sYear in aYears:
            try:
                sParent = sTWDParPrefix + str(sYear)
                oParCS = IPhysicalCardSet(sParent)
                aCS.append(oParCS.id)
                iYear = int(sYear)
                if iYear > iHighYear:
                    iHighYear = iYear
                if iYear < iLowYear:
                    iLowYear = iYear
            except SQLObjectNotFound:
                print('Unable to access "%s" - invalid year?' % sParent)
                # Fail
                aCS = []
                iLowYear = 9999
                iHighYear = 0
                break
    return aCS, iLowYear, iHighYear


def card_sets_with_progress(iWidth=79):
    """Generator which loops through the card child card sets, printing
       a progress bar"""
    iTotal = sutekh.PhysicalCardSet.select().count()
    iSeen = 0
    iFrac = 0
    iOldFrac = 0
    for oCS in sutekh.PhysicalCardSet.select():
        iSeen += 1
        # Ensure funky rounding on progress bar
        iFrac = (iSeen * iWidth + 2) / iTotal
        if iFrac > iOldFrac:
            sys.stderr.write('.')
            sys.stderr.flush()
        iOldFrac = iFrac
        if not oCS.parent:
            # skip top level card sets
            continue
        if oCS.parent.name == sTWDRemoved:
            # skip removed card sets
            continue
        yield oCS
    sys.stderr.write('\n')
    sys.stderr.flush()


def get_twd_url(sText):
    if not sText:
        return None
    if sTWDURLIndicator not in sText:
        return None
    for sLine in sText.split('\n'):
        if sTWDURLIndicator not in sLine:
            continue
        sLine = sLine.replace(sTWDURLIndicator, '')
        return sLine.strip()
    return None


def check_exact_match(oCS, sUrl):
    """Check that the exact url is in the card set"""
    for sLine in oCS.annotations.splitlines():
        sLine = sLine.strip()
        if sLine == sUrl:
            return True
    return False

