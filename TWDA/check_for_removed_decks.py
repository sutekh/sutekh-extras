#!/usr/bin/python3
# Check for any decks that have been removed from the TWDA

import sys
try:
    import sutekh
    from twd_utils import (sTWDURLIndicator, start_twd_db, parse_fixer_args,
                           sVEKNFranceTWDUrl, get_twd_file, sTWDRemoved,
                           sOldTWDURLIndicator)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

sBaseUrl = '%s %s' % (sTWDURLIndicator, sVEKNFranceTWDUrl)

if not start_twd_db():
    sys.exit(1)

oOpts = parse_fixer_args(sys.argv)

oTWDFile = get_twd_file()
if not oTWDFile:
    sys.exit(1)  # Failed to open file

aUrls = []

for sLine in oTWDFile:
    sLineLow = sLine.lower()
    if ('a href=#' not in sLineLow and
            'a href="http://www.vekn.fr/' not in sLineLow):
        continue
    if '<td colspan="2"><a href="#year' in sLineLow:
        continue
    if 'twd.htm#year' in sLineLow:
        continue
    if '<td><a href="#year' in sLineLow:
        continue
    if '<a href="#top">' in sLineLow:
        continue
    if 'ref=#>top</a>' in sLineLow:
        continue
    if 'href="#">top</a>' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks/twd.htm#top">' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks.htm">back</a>' in sLineLow:
        continue
    if '<a href="http://www.thelasombra.com/hall_of_fame.htm">' in sLineLow:
        continue
    if '<a href="http://www.vekn.fr/decks.htm">back</a>' in sLineLow:
        continue

    # TWD Entry
    sName = sLine.split('>')[1]
    sName = sName.split('<')[0].strip()
    sKey = sLine.split('=')[1].split('>')[0]
    if '#' in sKey:
        sKey = sKey.split('#')[1]
        sKey = '#' + sKey
    sUrl = sBaseUrl + sKey
    aUrls.append(sUrl)

for oCS in sutekh.PhysicalCardSet.select():
    if not oCS.parent:
        continue
    if not oCS.annotations:
        continue
    for sLine in oCS.annotations.splitlines():
        if sTWDURLIndicator not in sLine:
            continue
        if sOldTWDURLIndicator in sLine:
            continue
        if sLine not in aUrls:
            if oCS.parent.name != sTWDRemoved:
                print('Unable to find %s (child of %s) in twd.htm with %s' % (
                    oCS.name.encode('utf8'),
                    oCS.parent.name.encode('utf8'),
                    sLine))
        elif oCS.parent.name == sTWDRemoved:
            print('Found %s (currently removed) in twd.htm with %s' % (
                oCS.name.encode('utf8'), sLine))
