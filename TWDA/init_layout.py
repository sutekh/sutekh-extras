# Initialise the layout needed from split_twd from an existing database

import os
import sys

try:
    import sutekh
    from twd_utils import (start_twd_db, parse_fixer_args,
                           sTWDURLIndicator, get_twd_path,
                           sTWDParPrefix, sTWDRemoved, sTWDAHolderParent)
except ImportError as e:
    print e
    print 'Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?'
    sys.exit(1)

if not start_twd_db():
    sys.exit(1)

oOpts = parse_fixer_args(sys.argv)

aTopLevelCardSets = [x for x in
                     sutekh.PhysicalCardSet.select() if
                     (x.parent is None or x.parent.name == sTWDAHolderParent)]

sTWDPath = get_twd_path()


def make_dir(sName):
    sDir = os.path.join(sTWDPath, sName)
    if not os.path.exists(sDir):
        if oOpts.verbose:
            print 'Need to create %s' % sDir
        if oOpts.commit:
            os.mkdir(sDir)
    return sDir

make_dir('latest')

for oParCS in aTopLevelCardSets:
    if oParCS.name == sTWDAHolderParent:
        # skip this entirely
        continue
    elif oParCS.name == sTWDRemoved:
        sYear = sTWDRemoved.capitalize()
    elif oParCS.name.startswith(sTWDParPrefix):
        sYear = oParCS.name.replace(sTWDParPrefix, '')
    else:
        # Dunno what's happening here, so skip it
        print 'Unrecognised parent card set', oParCS.name
        continue
    sDir = make_dir(sYear)
    oParFilter = sutekh.core.Filters.ParentCardSetFilter([oParCS.name])
    aChildren = oParFilter.select(sutekh.PhysicalCardSet)
    for oChildCS in aChildren:
        for sLine in oChildCS.annotations.split('\n'):
            if sLine.startswith(sTWDURLIndicator):
                # Filename is the anchor part
                sName = sLine.split('#')[1].strip()
                sFile = os.path.join(sDir, sName)
                if not os.path.exists(sFile):
                    if oOpts.verbose:
                        print 'Need to create %s' % sFile
                    if oOpts.commit:
                        # Touch file
                        open(sFile, 'w').close()
