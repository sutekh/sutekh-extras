#!/usr/bin/python3
# Extract some statistics about card usage in the TWDA


import sys
import optparse
import textwrap
from sqlobject import SQLObjectNotFound
try:
    from sutekh.base.core.BaseTables import (MapPhysicalCardToPhysicalCardSet,
                                             PhysicalCardSet, AbstractCard)
    from sutekh.base.core.BaseAdapters import IAbstractCard, IPhysicalCardSet
    from sutekh.base.core.BaseFilters import (MultiPhysicalCardSetMapFilter,
                                              SpecificCardFilter, FilterAndBox,
                                              ParentCardSetFilter,
                                              MultiSpecificCardIdFilter)
    from sutekh.base.core.FilterParser import FilterParser
    from twd_utils import start_twd_db, sTWDURLIndicator
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


def parse_options(aArgs):
    """Handle option parsing"""
    sHelpText = "usage: %prog [options]\n\n" + '\n'.join(
        textwrap.wrap("Finds stats about usage of cards in the TWDA. "
                      "When multiple cards or multiple filters are specified, "
                      "finds card sets with ALL the given cards or matching "
                      "all the filters. For 'any of the cards' style queries, "
                      "use 'CardName=x or CardName=y' filters."))
    oOptParser = optparse.OptionParser(usage=sHelpText,
                                       version="%prog 0.1")
    oOptParser.add_option("-y", "--year", dest="year", default=None,
                          help="Limit to TWDs from the given year")
    oOptParser.add_option("-c", "--count", dest="count", default=1,
                          help="Only consider decks with at least"
                               " count copies")
    oOptParser.add_option("-u", "--url", dest="url", action="store_true",
                          default=False, help="Display TWDA url annotation")
    oOptParser.add_option("-f", "--filter", action="store_true",
                          dest="filter", default=False,
                          help="Remaining arguments are intepreted as filters"
                               " to apply. If not set, remaining arguments"
                               " are a list of cards to find. When specifying"
                               " filters, be sure to quote them correctly, so"
                               " they are parsed correctly, especially when"
                               " using multiple filters.")
    (oOpts, aRemaining) = oOptParser.parse_args(aArgs)
    if len(aRemaining) == 1:
        # sys.argv[0] is left in aRemaining
        if oOpts.filter:
            print("No arguments to interpret as filter")
        else:
            print("No arguments to interpret as cards")
        oOptParser.print_help()
        sys.exit(1)
    return oOpts, aRemaining[1:]


def print_stats_dict(dStats, dSeen, bShowUrl):
    iTotal = 0
    iDeckTotal = 0
    for sKey in sorted(dStats):
        print('Card Set %s' % sKey.encode('utf8'))
        if bShowUrl:
            oCS = IPhysicalCardSet(sKey)
            aAnnotations = oCS.annotations.splitlines()
            for sLine in aAnnotations:
                if sLine.startswith(sTWDURLIndicator):
                    print('     %s' % sLine.strip())
        for sCardName in sorted(dStats[sKey]):
            print('    %20s : %d' % (sCardName, dStats[sKey][sCardName]))
            iTotal += dStats[sKey][sCardName]
        if dSeen:
            oFilter = ParentCardSetFilter([sKey])
            iTotalDecks = oFilter.select(PhysicalCardSet).count()
            iDeckCount = len(dSeen[sKey])
            iDeckMax = max(dSeen[sKey].values())
            iDeckMin = min(dSeen[sKey].values())
            iDeckTotal += iDeckCount
            print('  Decks                 : %d / %d' % (iDeckCount,
                                                         iTotalDecks))
            print('     Percentage         : %.3f' % (
                (100.0 * iDeckCount) / iTotalDecks))
            fUsed = 0
            for iCardUsed in dStats[sKey].values():
                fUsed += float(iCardUsed)
            print('  Average per used deck : %.3f (max %d, min %d)' % (
                fUsed / iDeckCount, iDeckMax, iDeckMin))
    print('Total occurances : %d' % iTotal)
    if iDeckTotal > 0:
        print('Total Decks      : %d' % iDeckTotal)


def update_stats_dict(dStats, sCSName, sCardName):
    dStats.setdefault(sCSName, {})
    dStats[sCSName].setdefault(sCardName, 0)
    dStats[sCSName][sCardName] += 1


def clear_stale_stats(dStats, dParent, dSeen, aSeenSet, aSeenParSet):
    """Remove card sets we haven't seen recently from the list"""
    aToRemove = []
    for sCS in dStats:
        if sCS not in aSeenSet:
            aToRemove.append(sCS)
    for sCS in aToRemove:
        for sParCS in aSeenParSet:
            if sCS in dSeen[sParCS]:
                del dSeen[sParCS][sCS]
                # Loop over a copy, so we can delete 0 counts
                for sCard in list(dStats[sCS]):
                    dParent[sParCS][sCard] -= dStats[sCS][sCard]
                    if dParent[sParCS][sCard] == 0:
                        del dParent[sParCS][sCard]
        del dStats[sCS]
    aToRemove = []
    for sParCS in dSeen:
        if sParCS not in aSeenParSet:
            aToRemove.append(sParCS)
    for sParCS in aToRemove:
        del dSeen[sParCS]
        del dParent[sParCS]


def process_filter(oFilter, dStats, dParentStats, dSeenDecks, sTWDYear,
                   iCount):
    aNewNames = set([])  # List for next iteration of the loop
    aSeenParents = set([])  # Track parents to clean out
    aCards = []
    for oMapCard in oFilter.select(MapPhysicalCardToPhysicalCardSet):
        oCS = oMapCard.physicalCardSet
        sCSName = oCS.name
        sCardName = IAbstractCard(oMapCard).name
        sCSParName = oCS.parent.name
        if sTWDYear and sCSParName != sTWDYear:
            continue
        aCards.append(oMapCard)
        update_stats_dict(dStats, sCSName, sCardName)
        update_stats_dict(dParentStats, sCSParName, sCardName)
        dSeenDecks.setdefault(sCSParName, {})
        dSeenDecks[sCSParName][sCSName] = sum(dStats[sCSName].values())
        aNewNames.add(sCSName)
        aSeenParents.add(sCSParName)
    for oMapCard in aCards:
        oCS = oMapCard.physicalCardSet
        sCSName = oCS.name
        sCardName = IAbstractCard(oMapCard).name
        sCSParName = oCS.parent.name
        if sCSName in dStats:
            if sCardName in dStats[sCSName] and \
                    dStats[sCSName][sCardName] < iCount:
                iCSCount = dStats[sCSName][sCardName]
                dParentStats[sCSParName][sCardName] -= iCSCount
                dSeenDecks[sCSParName][sCSName] -= iCSCount
                del dStats[sCSName][sCardName]
                if dParentStats[sCSParName][sCardName] == 0:
                    del dParentStats[sCSParName][sCardName]
                if dSeenDecks[sCSParName][sCSName] == 0:
                    del dSeenDecks[sCSParName][sCSName]
                if len(dSeenDecks[sCSParName]) == 0:
                    del dSeenDecks[sCSParName]
            if len(dStats[sCSName]) == 0:
                del dStats[sCSName]
                aNewNames.remove(sCSName)
            if len(dParentStats[sCSParName]) == 0:
                del dParentStats[sCSParName]
                aSeenParents.remove(sCSParName)
    return aNewNames, aSeenParents


def find_cards(aCards, sTWDYear, iCount, bShowUrl):
    dStats = {}
    dParentStats = {}
    dSeenDecks = {}
    aNames = [x.name for x in PhysicalCardSet.select()]

    for sCardName in aCards:
        try:
            oCard = IAbstractCard(sCardName)
        except SQLObjectNotFound:
            print('Unable to identify card matching %s' % sCardName)
            continue
        except UnicodeDecodeError:
            try:
                oCard = IAbstractCard(sCardName.decode('utf8'))
            except SQLObjectNotFound:
                print('Unable to identify card matching %s'
                      % sCardName.decode('utf8'))
                continue
        print('Searching TWDA decks for %s' % sCardName)

        oCardFilter = SpecificCardFilter(oCard)
        oMapFilter = MultiPhysicalCardSetMapFilter(aNames)
        oFilter = FilterAndBox([oCardFilter, oMapFilter])

        aNewNames, aSeenParents = process_filter(oFilter, dStats,
                                                 dParentStats, dSeenDecks,
                                                 sTWDYear, iCount)
        aNames = list(aNewNames)
        clear_stale_stats(dStats, dParentStats, dSeenDecks, aNewNames,
                          aSeenParents)

    print_stats_dict(dStats, None, bShowUrl)
    print_stats_dict(dParentStats, dSeenDecks, None)


def find_filter(aFilters, sTWDYear, iCount, bShowUrl):
    dStats = {}
    dParentStats = {}
    dSeenDecks = {}

    aNames = [x.name for x in PhysicalCardSet.select()]

    for sFilter, oFilter in aFilters:
        print('Searching TWDA decks for %s' % sFilter)

        oMapFilter = MultiPhysicalCardSetMapFilter(aNames)
        # Find all cards that match the filter, since we filter on
        # physical cards
        oAllFilter = MultiSpecificCardIdFilter([x.id for x in
                                                oFilter.select(AbstractCard)])
        oMyFilter = FilterAndBox([oAllFilter, oMapFilter])

        aNewNames, aSeenParents = process_filter(oMyFilter, dStats,
                                                 dParentStats, dSeenDecks,
                                                 sTWDYear, iCount)
        aNames = list(aNewNames)
        clear_stale_stats(dStats, dParentStats, dSeenDecks, aNewNames,
                          aSeenParents)

    print_stats_dict(dStats, None, bShowUrl)
    print_stats_dict(dParentStats, dSeenDecks, None)


if __name__ == "__main__":
    oOpts, aRemaining = parse_options(sys.argv)
    if not start_twd_db():
        print('Environment not initialised')
        sys.exit(1)
    if oOpts.year:
        try:
            sTWDYear = 'TWDA %s' % oOpts.year
            _ = IPhysicalCardSet(sTWDYear)
        except SQLObjectNotFound:
            print('Unable to find TWDA holder for year %s' % oOpts.year)
            sys.exit(1)
    else:
        sTWDYear = None
    try:
        iCount = int(oOpts.count)
        if iCount < 1 or iCount > 50:
            print('Invalid value for count: %d - need a value in [1, 50]' % iCount)
            sys.exit(1)
    except ValueError:
        print('Invalud value for count: %s - expected a integer' % oOpts.count)
        sys.exit(1)
    if not oOpts.filter:
        find_cards(aRemaining, sTWDYear, int(oOpts.count), oOpts.url)
    else:
        # filter mode
        oParser = FilterParser()
        aFilters = []
        for sFilter in aRemaining:
            try:
                oFilter = oParser.apply(sFilter).get_filter()
                aFilters.append((sFilter, oFilter))
            except Exception as e:
                print('Invalid Filter string: %s' % sFilter)
                print('Perhaps quote the filter?')
                print(e)
                print
        if len(aFilters) > 0:
            find_filter(aFilters, sTWDYear, int(oOpts.count), oOpts.url)
        else:
            print('No valid filters found')
