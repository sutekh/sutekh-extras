#!/usr/bin/python3
# Extract info on clan usage from the TWDA

import sys
import optparse
try:

    from sutekh.base.core.BaseTables import MapPhysicalCardToPhysicalCardSet
    from sutekh.base.core.BaseAdapters import IAbstractCard
    from sutekh.base.core.FilterParser import FilterParser
    from sutekh.base.core.BaseFilters import (PhysicalCardSetFilter,
                                              FilterAndBox,
                                              MultiCardTypeFilter)
    from twd_utils import start_twd_db, select_years, card_sets_with_progress
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)


def parse_options(aArgs):
    """Handle option parsing"""
    sHelpText = ("usage: %prog [year] [year2] ... [options]\n\n"
                 "Finds stats about clan usage in the TWDA.\n"
                 " The imbued are considered a single clan here.")
    oOptParser = optparse.OptionParser(usage=sHelpText,
                                       version="%prog 0.1")
    aRules = ["split", "twd"]
    oOptParser.add_option("-r", "--rule", dest="rule", default='twd',
                          choices=aRules,
                          help="Rule used for selection [%s]\n"
                               "'twd' - same as the twd - ties count as "
                               "both clans.\n"
                               "'split' - ties are classed as"
                               " Clan A / Clan B" % "|".join(aRules))
    oOptParser.add_option("-g", "--group", dest="group", default=False,
                          action="store_true",
                          help="Split clan occurances by group")
    oOptParser.add_option("-s", "--show", dest="show", default=False,
                          action="store_true",
                          help="Show the names of all the decks for"
                               " each classification.")
    oOptParser.add_option("-l", "--filter", dest="filter", default=None,
                          help="Specify a filter to apply to the cards"
                               " checked, to allow restricting to a specific"
                               " expansion or so forth")
    (oOpts, aRemaining) = oOptParser.parse_args(aArgs)
    if len(aRemaining) == 1:
        aYears = None
    else:
        aYears = aRemaining[1:]
    return oOpts, aYears


def find_card_stats(aParCS, oFilter, sRule):
    oCardFilter = MultiCardTypeFilter(['Vampire', 'Imbued'])
    dClans = {}
    dClanGrps = {}
    dDecks = {}
    for oCS in card_sets_with_progress(79):
        sEncName = oCS.name.encode('utf8')
        if oCS.parentID not in aParCS:
            # Only update dStats for the card sets of interest
            continue
        oBaseFilter = FilterAndBox([PhysicalCardSetFilter(oCS.name),
                                   oCardFilter])
        if oFilter:
            oJointFilter = FilterAndBox([oBaseFilter, oFilter])
        else:
            oJointFilter = oBaseFilter
        aCards = [IAbstractCard(x) for x in
                  oJointFilter.select(MapPhysicalCardToPhysicalCardSet)]
        dCSClans = {}
        aGroups = set()
        for oCard in aCards:
            if oCard.clan:
                sClan = [x.name for x in oCard.clan][0]
            else:
                sClan = 'Imbued'
            dCSClans.setdefault(sClan, 0)
            dCSClans[sClan] += 1
            aGroups.add(oCard.group)
        iMax = 0
        aClans = []
        for sClan, iClanNum in dCSClans.items():
            if iClanNum > iMax:
                iMax = iClanNum
                aClans = [sClan]
            elif iClanNum == iMax:
                aClans.append(sClan)
        if iMax < 5:
            # Skip overly split decks
            continue
        aGroups.discard(None)
        aGroups.discard(-1)
        if len(aGroups) > 1:
            # Rethink this when we pass 100 groups
            iGroup = 100 * min(aGroups) + max(aGroups)
        else:
            iGroup = max(aGroups)

        if sRule == 'split':
            sClan = " / ".join(sorted(aClans))
            dClans.setdefault(sClan, 0)
            dClans[sClan] += 1
            dClanGrps.setdefault(sClan, {})
            dClanGrps[sClan].setdefault(iGroup, 0)
            dClanGrps[sClan][iGroup] += 1
            dDecks.setdefault(sClan, [])
            dDecks[sClan].append(sEncName)
        else:
            for sClan in aClans:
                dClans.setdefault(sClan, 0)
                dClans[sClan] += 1
                dClanGrps.setdefault(sClan, {})
                dClanGrps[sClan].setdefault(iGroup, 0)
                dClanGrps[sClan][iGroup] += 1
                dDecks.setdefault(sClan, [])
                dDecks[sClan].append(sEncName)
    return dClans, dClanGrps, dDecks


def print_stats(dClans, dClanGrps, dDecks, iLowYear, iHighYear):
    for sClan, iNumber in sorted(dClans.items(), key=lambda x: -x[1]):
        print('{0:<22s} : {1:4d}'.format(sClan, iNumber))
        if sClan in dClanGrps:
            for iGroup, iCount in sorted(dClanGrps[sClan].items(),
                                         key=lambda x: -x[1]):
                if iGroup > 100:
                    sGroup = '{0:d} & {1:d}'.format(iGroup // 100,
                                                    iGroup % 100)
                else:
                    sGroup = '{0:d}'.format(iGroup)
                print('     {0:^17s} : {1:4d}'.format(sGroup, iCount))
        if sClan in dDecks:
            for sDeck in sorted(dDecks[sClan]):
                print(sDeck)
        print()


if __name__ == "__main__":
    if not start_twd_db():
        print('Environment not initialised')
        sys.exit(1)
    oOpts, aYears = parse_options(sys.argv)
    aParCS, iLowYear, iHighYear = select_years(aYears)
    if not aParCS:
        print('Empty year card set list')
        sys.exit(1)
    if oOpts.filter:
        oParser = FilterParser()
        oFilter = oParser.apply(oOpts.filter).get_filter()
    else:
        oFilter = None
    dClans, dClanGrps, dDecks = find_card_stats(aParCS, oFilter, oOpts.rule)
    if not oOpts.group:
        dClanGrps = {}
    if not oOpts.show:
        dDecks = {}
    print_stats(dClans, dClanGrps, dDecks, iLowYear, iHighYear)
