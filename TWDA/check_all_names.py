import sys

try:
    import sutekh
    from sutekh.base.Utility import unaccent
    from twd_utils import (start_twd_db, sTWDURLIndicator, sVEKNFranceTWDUrl,
                           get_twd_file, sTWDParPrefix)
except ImportError as e:
    print e
    print 'Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?'
    sys.exit(1)


sBaseUrl = '%s %s' % (sTWDURLIndicator, sVEKNFranceTWDUrl)

if '--help' in sys.argv:
    print 'Usage: %s [--skip-unicode] [--diff]' % sys.argv[0]
    print 'List deck names that differ between the database' \
          ' and the twd.htm list'
    print 'With --skip-unicode , ignore unicode only differences'
    print 'With --diff , use difflib to show the differences'
    sys.exit(0)

if not start_twd_db():
    sys.exit(1)

oTWDFile = get_twd_file()
if not oTWDFile:
    sys.exit(1)  # Failed to open file

bHideUnicode = False
if '--skip-unicode' in sys.argv:
    bHideUnicode = True

import difflib

oDiff = difflib.Differ()

bDiff = False
if '--diff' in sys.argv:
    bDiff = True

dStats = {}

for sLine in oTWDFile:
    sLineLow = sLine.lower()
    if 'a href="#' not in sLineLow:
        continue
    if '<td><a href="#year' in sLineLow:
        continue
    if '<a href="#top">' in sLineLow:
        continue
    if '<a href="#">' in sLineLow:
        continue
    if '<td colspan="2"><a href="#year' in sLineLow:
        continue
    if 'twd.htm#year' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks/twd.htm#top">' in sLineLow:
        continue
    if '<a href="http://www.veknfrance.com/decks.htm">back</a>' in sLineLow:
        continue
    if '<a href="http://www.thelasombra.com/hall_of_fame.htm">' in sLineLow:
        continue
    if '<a href="http://www.vekn.fr/decks/twd.htm#top">' in sLineLow:
        continue
    # TWD Entry
    sMatchReason = 'unknown means'
    sName = sLine.split('>')[1]
    sName = sName.split('<')[0].strip().decode('utf8')
    sKey = sLine.split('"')[1]
    sUrl = sBaseUrl + sKey
    # The new vekn list uses &amp; everywhere
    if '&amp;' in sName:
        sName = sName.replace('&amp;', '&')
    try:
        oCS = sutekh.PhysicalCardSet.byName(sName)
    except Exception as e:
        sFiltName = sName.encode('ascii',
                                 'replace').replace('?', '_').decode('utf8')
        oFilter = sutekh.CardSetNameFilter(sFiltName)
        aCS = list(oFilter.select(sutekh.PhysicalCardSet))
        if len(aCS) != 1:
            oFilter = sutekh.CardSetAnnotationsFilter(sUrl)
            aUrlCS = list(oFilter.select(sutekh.PhysicalCardSet))
            if len(aUrlCS) != 1:
                print "======================"
                print sLine, "doesn't match a unique card set"
                print 'Wildcard name', sFiltName
                if aCS:
                    print 'Name candidates are', aCS
                if aUrlCS:
                    print 'TWD Url Candidates are', aUrlCS
                if not aCS and not aUrlCS:
                    print 'No candidates'
                print sUrl
                print "======================="
                continue
            else:
                oCS = aUrlCS[0]
                sMatchReason = 'TWD url'
        else:
            oCS = aCS[0]
            sMatchReason = 'wildcard Name match'

    if sName != oCS.name:
        sMsg = None
        oRes = list(oDiff.compare([sName], [oCS.name]))
        if unaccent(sName) != unaccent(oCS.name):
            sMsg = "Matching %s with %s by %s" % (sName, oCS.name,
                                                  sMatchReason)
        elif not bHideUnicode:
            sMsg = "Unicode differences between TWD and database:"
            sMsg += " Matching %s (%s) with %s (%s) by %s" % (
                sName, unaccent(sName), oCS.name, unaccent(oCS.name),
                sMatchReason)
        if sMsg:
            print sMsg.encode('ascii', 'replace')
            if bDiff:
                print 'Diff:'
                print '\n'.join(oRes).encode('ascii', 'replace')
    if oCS.parent:
        if oCS.parent.name in dStats:
            dStats[oCS.parent.name] += 1
        else:
            dStats[oCS.parent.name] = 1

    if oCS.annotations and sUrl in oCS.annotations:
        continue
    else:
        if oCS.annotations:
            if sTWDURLIndicator in oCS.annotations:
                print "================================"
                print 'Inconsistent TWD url'
                print oCS.name.encode('utf8'), oCS.annotations, sUrl
                print "==============================="
            else:
                print "================================"
                print 'No TWD url'
                print oCS.name.encode('utf8'), oCS.annotations, sUrl
                print "==============================="
        else:
            print '========================'
            print 'No annotations'
            print oCS.name.encode('utf8'), sUrl
            print '========================'

for sKey in sorted(dStats):
    print '%s  : %d' % (sKey.replace(sTWDParPrefix, ''), dStats[sKey])
