#!/usr/bin/python3
# Check for card sets that are now illegal
import sys

try:
    import sutekh
    from sutekh.SutekhUtility import is_crypt_card
    from sutekh.base.core.BaseAdapters import IKeyword
    from twd_utils import start_twd_db, sTWDAHolderParent
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

# Special cases

if not start_twd_db():
    sys.exit(1)  # Failure

dStats = {'Too few vampires': [],
          'Library too small': [],
          'Library too large': [],
          'Illegal/Banned cards': [],
          'Grouping violation': [],
          }

oNotLegal = IKeyword('not for legal play')

for oCS in sutekh.PhysicalCardSet.select():
    if oCS.parent is None or oCS.parent.name == sTWDAHolderParent:
        # Skip holders
        continue
    # We check for the following
    # Banned cards
    # Wrong crypt size
    iCryptCount = 0
    iLibraryCount = 0
    aNotLegal = set()
    aCards = [x.abstractCard for x in oCS.cards]
    iMinGrp = 99999
    iMaxGrp = 0
    bRemoved = False
    if oCS.parent.name == 'REMOVED':
        bRemoved = True
    for oCard in aCards:
        if is_crypt_card(oCard):
            iCryptCount += 1
            if oCard.group == -1:
                continue  # skip any groups
            if oCard.group < iMinGrp:
                iMinGrp = oCard.group
            if oCard.group > iMaxGrp:
                iMaxGrp = oCard.group
        else:
            iLibraryCount += 1
        if oNotLegal in oCard.keywords:
            aNotLegal.add(oCard.name)
    if iCryptCount < 12:
        dStats['Too few vampires'].append((oCS.name, '%d' % iCryptCount,
                                           bRemoved))
    if iLibraryCount < 60:
        dStats['Library too small'].append((oCS.name, '%d' % iLibraryCount,
                                            bRemoved))
    if iLibraryCount > 90:
        dStats['Library too large'].append((oCS.name, '%d' % iLibraryCount,
                                            bRemoved))
    if aNotLegal:
        dStats['Illegal/Banned cards'].append((oCS.name,
                                               ' '.join(sorted(aNotLegal)),
                                               bRemoved))
    if (iMaxGrp - iMinGrp) >= 2:
        dStats['Grouping violation'].append((oCS.name, 'min: %d  max: %d' % (
            iMinGrp, iMaxGrp), bRemoved))


for sType in dStats:
    print('%d No longer legal decks (%s)' % (len(dStats[sType]), sType))
    for sName, sDetails, bRemoved in sorted(dStats[sType],
                                            key=lambda x: (x[0][-4:], x)):
        # We do the unicode to ascii dance so the output is
        # pipe-able
        sName = sName.encode('ascii', 'replace').decode('utf8')
        if bRemoved:
            print('Name: %s (removed from TWDA)' % sName)
        else:
            print('Name: %s' % sName)
        print('    ', sDetails)
