# Find and list all authors, grouping those that differ only by
# unicode representation
import sys
import unicodedata

try:
    import sutekh
    from twd_utils import start_twd_db, sTWDAHolderParent
except ImportError as e:
    print e
    print 'Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?'
    sys.exit(1)

# Special cases

if not start_twd_db():
    sys.exit(1)  # Failure


def to_ascii(sText):
    """Normalise text to ascii variant"""
    return unicodedata.normalize('NFKD', sText).encode('ascii', 'ignore')


dAuthors = {}
dDecks = {}
bListDecks = False
bOnlyMultiples = False

if len(sys.argv) > 1:
    # Check arguments
    if '--help' in sys.argv:
        print 'Usage: %s [--decks] [--only-multiples]' % sys.argv[0]
        print 'List the authors in the TWD database'
        print 'With --decks, also list associated decks'
        print 'With --only-multiples, list those that look like possible'
        print '   unicode mismatches'
        sys.exit(0)

    if '--decks' in sys.argv:
        bListDecks = True

    if '--only-multiples' in sys.argv:
        bOnlyMultiples = True

for oCS in sutekh.PhysicalCardSet.select():
    if oCS.parent is None or oCS.parent.name == sTWDAHolderParent:
        # Skip holders
        continue
    sAuthor = to_ascii(oCS.author).title().strip()
    dAuthors.setdefault(sAuthor, set())
    dAuthors[sAuthor].add(oCS.author)
    dDecks.setdefault(oCS.author, [])
    dDecks[oCS.author].append(oCS.name)

for sAuthor in sorted(dAuthors):
    aNames = dAuthors[sAuthor]
    sSetName = list(aNames)[0]  # First name
    if bOnlyMultiples and len(aNames) < 2:
        continue
    if len(aNames) > 1:
        print sAuthor
        print '    Multiple candidates'
        for sName in aNames:
            print '      ', sName.encode('utf8')
    elif sSetName.title() != sAuthor:
        print sAuthor
        print '   canonical form', sSetName.encode('utf8')
    else:
        print sAuthor
    if bListDecks:
        print 'Decks:'
        for sName in aNames:
            print ',  '.join([x.encode('utf8') for x in dDecks[sName]])
