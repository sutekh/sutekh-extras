#!/usr/bin/python3
# Append the TWD Date to the card set annotations for the TWD
# atchives

import sys
import re
import datetime

try:
    import sutekh
    from twd_utils import (sTWDDateIndicator, start_twd_db,
                           parse_fixer_args, sTWDAHolderParent)
except ImportError as e:
    print(e)
    print('Failed to import sutekh & twd_utils - is PYTHONPATH set correctly?')
    sys.exit(1)

if not start_twd_db():
    sys.exit(1)

oOpts = parse_fixer_args(sys.argv)

aDateFormats = [
        (re.compile('^[0-9].*-[A-Za-z].*-[12][0-9][0-9][0-9]$'), '%d-%B-%Y'),
        (re.compile('^[0-9].*-[A-Za-z].*-[12][0-9][0-9][0-9]$'), '%d-%b-%Y'),
        (re.compile('^[0-9].*-[A-Za-z].*-[0-9][0-9]$'), '%d-%b-%y'),
        (re.compile('^[0-9].*-[A-Za-z].*-[0-9][0-9]$'), '%d-%B-%y'),
        (re.compile('^[0-9].*/[0-9].*/[12][0-9][0-9][0-9]$'), '%d/%m/%Y'),
        (re.compile('^[0-9].*/[0-9].*/[12][0-9][0-9][0-9]$'), '%m/%d/%Y'),
        (re.compile('^[0-9].*/[0-9].*/[0-9][0-9]$'), '%d/%m/%y'),
        (re.compile('^[0-9].*\.[0-9].*\.[12][0-9][0-9][0-9]$'), '%d %m %Y'),
        (re.compile('^[0-9].*\.[0-9].*\.[0-9][0-9]$'), '%d %m %y'),
        (re.compile('^[0-9].*/[A-Za-z]*/[12][0-9][0-9][0-9]$'), '%d/%B/%Y'),
        (re.compile('^[0-9].*/[A-Za-z]*/[12][0-9][0-9][0-9]$'), '%d/%b/%Y'),
        (re.compile('^[0-9].* [12][0-9][0-9][0-9]$'), '%d %B %Y'),
        (re.compile('^[0-9].* [12][0-9][0-9][0-9]$'), '%d %b %Y'),
        (re.compile('^[0-9].* [0-9][0-9]$'), '%d %B %y'),
        (re.compile('^[0-9].* [0-9][0-9]$'), '%d %b %y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*[,\.] [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*,[12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [0-9][0-9]$'), '%B %d %y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]*[,\.] [0-9][0-9]$'), '%b %d %y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]* [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]* [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]* [0-9][0-9]$'), '%B %d %y'),
        (re.compile('^[A-Za-z][a-z]* [0-9]* [0-9][0-9]$'), '%b %d %y'),
        (re.compile('^[A-Za-z][a-z]*, [0-9]* [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile('^[A-Za-z][a-z]*, [0-9]* [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile('^[A-Za-z][a-z]*, [0-9]* [0-9][0-9]$'), '%B %d %y'),
        (re.compile('^[A-Za-z][a-z]*, [0-9]* [0-9][0-9]$'), '%b %d %y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*, [12][0-9][0-9][0-9] [0-9]*[ ]*(am|pm)$'),
            '%B %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*(st|nd|rd|th)[,\.] [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9]$'),
            '%b %d %Y'),
        (re.compile(
            '^[A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9] -- [A-Za-z][a-z]* [0-9]*(st|nd|rd|th) [12][0-9][0-9][0-9]$'),
            '%B %d %Y'),
        ]

aDays = ['monday ', 'tuesday ', 'wednesday ', 'thursday ', 'friday ',
         'saturday ', 'sunday ']


def normalise(sDate):
    """Standardise some problematic cases"""
    if ' -- ' in sDate:
        # We take the first date of ranges
        sDate = sDate.split(' -- ')[0]
    sDate = sDate.replace('.', ' ').replace(',', ' ')
    sDate = sDate.replace('1st', '1')
    sDate = sDate.replace('2nd', '2')
    sDate = sDate.replace('3rd', '3')
    for iDigit in range(0, 10):
        sDate = sDate.replace('%sth' % iDigit, '%s' % iDigit)
    sDate = sDate.replace(' of ', ' ')
    sDate = sDate.replace(' day ', ' ')
    sDate = sDate.replace('  ', ' ').replace('  ', ' ').replace('  ', ' ')
    if 'am' in sDate or 'pm' in sDate:
        oTimeRgx = re.compile(' [0-9]*[ ]*(am|pm)')
        sDate = oTimeRgx.sub('', sDate)
    return sDate

for oCS in sutekh.PhysicalCardSet.select():
    if oCS.parent is None or oCS.parent.name == sTWDAHolderParent:
        # Skip TWD holders
        continue
    if oCS.annotations and sTWDDateIndicator in oCS.annotations:
        # Have date, so continue
        continue
    else:
        if not oCS.comment:
            if oOpts.verbose:
                print('No description for %s' % oCS.name)
            continue
        sData = oCS.comment
        sCSDate = None
        for sLine in sData.split('\n'):
            if 'date:' in sLine.lower():
                sLine = sLine.split(':', 1)[1]
            if 'date of event:' in sLine.lower():
                sLine = sLine.split(':', 1)[1]
            if 'when:' in sLine.lower():
                sLine = sLine.split(':', 1)[1]
            for sDay in aDays:
                if sDay in sLine.lower():
                    sLine = sLine.lower().replace(sDay, '')
                    break
            for oDateRegexp, sDateFormat in aDateFormats:
                oMatch = oDateRegexp.match(sLine.strip())
                if oMatch:
                    sDateString = normalise(oMatch.group())
                    try:
                        oTheDate = datetime.datetime.strptime(sDateString,
                                                              sDateFormat)
                        if oTheDate:
                            sCSDate = '%s %s' % (sTWDDateIndicator,
                                                 oTheDate.strftime('%Y-%m-%d'))
                            sMonth = oTheDate.strftime('%b')
                            tCSDate = (oMatch.group(), sDateString)
                            break
                    except ValueError:
                        pass
        if not sCSDate:
            if oOpts.verbose:
                print('Unable to determine date for %s' % oCS.name)
        else:
            if oCS.annotations:
                if sMonth not in oCS.name:
                    print('Possibly incorrect date, got month %s' % sMonth)
                sAction = 'setting %s to %s (from %s)' % (oCS.name, sCSDate,
                                                          tCSDate)
                if oOpts.commit:
                    print(sAction)
                    oCS.annotations += '\n%s' % sCSDate
                    oCS.syncUpdate()
                else:
                    print('Would do %s' % sAction)
