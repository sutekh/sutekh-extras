#!/usr/bin/python3

import sys
import unicodedata

from sutekh.base.Utility import move_articles_to_back, to_ascii
from sutekh import start

from vtes_csv import read_crypt, format_crypt_card, read_lib, format_lib_card


def sort_key(sName):
    """This approximates the csv file ordering"""
    #sName = move_articles_to_back(sName).decode('utf8')
    sName = to_ascii(sName).encode('ascii', 'ignore')
    return sName.lower()

start()

dCards = read_crypt(sys.argv[1])
dCards.update(read_lib(sys.argv[2]))

for sName in sorted(dCards, key=sort_key):
    if dCards[sName]['Type'] in ['Imbued', 'Vampire']:
        print(format_crypt_card(dCards[sName]))
    else:
        print(format_lib_card(dCards[sName]))
    print()
