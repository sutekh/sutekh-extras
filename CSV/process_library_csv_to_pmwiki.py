#!/usr/bin/python3

import sys

from sutekh import start

from vtes_csv import read_lib, PmWikiReportFormatter

start()

formatter = PmWikiReportFormatter(["Player 1", "Player 2", "Player 3"])


dCards = read_lib(sys.argv[1])

for sName in sorted(dCards):
    print(formatter.format_lib_card(dCards[sName]))
    print()
