# vtes_csv.py
# Helper utilities for reading csv files

import csv
import enum

from sutekh.base.Utility import move_articles_to_front
from sutekh.core.Abbreviations import Virtues, Creeds



def _add_to_dict(sName, dEntry, dCards):
    """Helper function to ensure we have unique keys in the dict that
       sort as expected in the face of duplicate names from advanced
       vampires and so forth"""
    sKey = sName.lower()
    if sKey not in dCards:
        dCards[sKey] = dEntry
    else:
        # This is to ensure we sort correctly in the case of advanced vamps
        iOffset = 0
        while sKey in dCards:
            sKey = '%s%d' % (sName.lower(), iOffset)
            iOffset += 1
        dCards[sKey] = dEntry


def read_crypt(sFilename, bHeader=True):
    """Read the crypt csv file into a dictionary"""
    # Indexes to the cypt CSV file
    tExpected = (
        'Id',
        'Name',
        'Aka',
        'Type',
        'Clan',
        'Adv',
        'Group',
        'Capacity',
        'Disciplines',
        'Card Text',
        'Set',
        'Title',
        'Banned',
        'Artist',
    )
    dColumns = {}

    oCSVFile = open(sFilename, 'r')
    oReader = iter(csv.reader(oCSVFile))
    if bHeader:
        for (iIndex, sName) in enumerate(next(oReader)):
            dColumns[sName] = iIndex
        for sName in tExpected:
            if sName not in dColumns:
                dColumns[sName] = -1
    else:
        for (iIndex, sName) in enumerate(tExpected):
            dColumns[sName] = iIndex

    oCSVFile = open(sFilename, 'r')
    oReader = iter(csv.reader(oCSVFile))
    if bHeader:
        next(oReader)  # Skip the header

    dCards = {}

    bFailed = False

    # Crypt layout is
    # Name, Type, Clan, Adv, Group, Capacity, Disciplines, Card Text, Set,
    # Title, Banned, Artist, discipline flags
    for oRow in oReader:
        if not oRow:
            # Skip blank lines
            continue
        try:
            sName = move_articles_to_front(oRow[dColumns['Name']].strip())
            # Replace any errata braces in the card name
            sName = sName.replace('{', '').replace('}', '')
            dEntry = {}
            dEntry['Name'] = sName
            if dColumns['Aka'] > 0 and oRow[dColumns['Aka']].strip():
                dEntry['AKA'] = oRow[dColumns['Aka']].strip()
            dEntry['Type'] = oRow[dColumns['Type']].strip()
            if dEntry['Type'] == 'Imbued':
                dEntry['Creed'] = oRow[dColumns['Clan']].strip()
            else:
                dEntry['Clan'] = oRow[dColumns['Clan']].strip()
            dEntry['Adv'] = oRow[dColumns['Adv']].strip()
            dEntry['Group'] = oRow[dColumns['Group']].strip()
            if dEntry['Type'] == 'Imbued':
                dEntry['Life'] = oRow[dColumns['Capacity']].strip()
            else:
                dEntry['Capacity'] = oRow[dColumns['Capacity']].strip()

            sDisciplines = oRow[dColumns['Disciplines']].strip()
            if sDisciplines:
                # Split into imbued and vampires
                if dEntry['Type'] == 'Imbued':
                    dEntry['Virtue'] = sDisciplines
                else:
                    dEntry['Discipline'] = sDisciplines
            else:
                dEntry['Discipline'] = '-none-'

            dEntry['Text'] = oRow[dColumns['Card Text']].replace(' [MERGED]', '\n[MERGED]').strip()

            if dColumns['Set'] > -1:
                dEntry['Set'] = oRow[dColumns['Set']].strip()
            else:
                dEntry['Set'] = None
            # We assume unknown sets are playtesting
            if not dEntry['Set']:
                dEntry['Set'] = 'Playtest'

            if dColumns['Banned'] > 0:
                if oRow[dColumns['Banned']].strip():
                    dEntry['Banned'] = ('{Added to the V:EKN banned list in %s.}' %
                            oRow[dColumns['Banned']].strip())

            if dColumns['Artist'] > 0:
                dEntry['Artist'] = oRow[dColumns['Artist']].strip()
            else:
                dEntry['Artist'] = None
            if not dEntry['Artist']:
                dEntry['Artist'] = 'unknown'

            if dEntry['Adv'] == 'Advanced':
                dEntry['Name'] += ' (Adv)'
        except IndexError as err:
            print("Failed on line", oRow)
            print("Error", err)
            bFailed = True
            continue

        _add_to_dict(sName, dEntry, dCards)

    if bFailed:
        return None

    return dCards


def read_lib(sFilename, bHeader=True):
    """Read the library csv file into a dictionary"""
    # Indexes to the cypt CSV file
    tExpected = (
        'Id',
        'Name',
        'Aka',
        'Type',
        'Clan',
        'Discipline',
        'Pool Cost',
        'Blood Cost',
        'Conviction Cost',
        'Burn Option',
        'Card Text',
        'Flavor Text',
        'Set',
        'Banned',
        'Artist',
        'Capacity',
    )
    dColumns = {}

    oCSVFile = open(sFilename, 'r')
    oReader = iter(csv.reader(oCSVFile))
    if bHeader:
        for (iIndex, sName) in enumerate(next(oReader)):
            dColumns[sName] = iIndex
        for sName in tExpected:
            if sName not in dColumns:
                dColumns[sName] = -1
    else:
        for (iIndex, sName) in enumerate(tExpected):
            dColumns[sName] = iIndex

    dCards = {}

    bFailed = False

    # Library layout is
    # Name, Type, Clan, Discipline, Pool Cost, Blood Cost, Conviction Cost,
    # Burn Option, Card Text, Flavour Text, Set, Requirement, Banned, Artist
    for oRow in oReader:
        if not oRow:
            # Skip blank lines
            continue
        try:
            sName = move_articles_to_front(oRow[dColumns['Name']].strip())
            # Replace any errata braces in the card name
            sName = sName.replace('{', '').replace('}', '')
            dEntry = {}
            dEntry['Name'] = sName
            if dColumns['Aka'] > 0 and oRow[dColumns['Aka']].strip():
                dEntry['AKA'] = oRow[dColumns['Aka']].strip()
            dEntry['Type'] = oRow[dColumns['Type']].strip()

            sClan = oRow[dColumns['Clan']].strip()
            if sClan:
                try:
                    Creeds.canonical(sClan)
                    dEntry['Creed'] = sClan
                except KeyError:
                    dEntry['Clan'] = sClan
            sDisc = oRow[dColumns['Discipline']].strip()
            if sDisc:
                try:
                    sTestDis = sDisc.split('/')[0]
                    Virtues.canonical(sTestDis)
                    dEntry['Virtue'] = sDisc
                except KeyError:
                    dEntry['Discipline'] = sDisc
            sPool = oRow[dColumns['Pool Cost']].strip()
            sBlood = oRow[dColumns['Blood Cost']].strip()
            sConv = oRow[dColumns['Conviction Cost']].strip()
            if sPool:
                dEntry['Cost'] = '%s pool' % sPool
            elif sBlood:
                dEntry['Cost'] = '%s blood' % sBlood
            elif sConv:
                dEntry['Cost'] = '%s Conviction' % sConv

            if oRow[dColumns['Burn Option']].strip():
                dEntry['Burn Option'] = '1'

            dEntry['Text'] = oRow[dColumns['Card Text']].replace('\r\n', '\n').strip()

            if dColumns['Set'] > 0:
                dEntry['Set'] = oRow[dColumns['Set']].strip()
            else:
                 dEntry['Set'] = None
            # We assume unknown sets are playtesting
            if not dEntry['Set']:
                dEntry['Set'] = 'Playtest'

            if dColumns['Banned'] > 0:
                if oRow[dColumns['Banned']].strip():
                    dEntry['Banned'] = ('{Added to the V:EKN banned list in %s.}' %
                            oRow[dColumns['Banned']].strip())

            if dColumns['Artist'] > 0:
                dEntry['Artist'] = oRow[dColumns['Artist']].strip()
            else:
                dEntry['Artist'] = None
            if not dEntry['Artist']:
                dEntry['Artist'] = 'unknown'
        except IndexError as err:
            print("Failed on line", oRow)
            print("Error", err)
            bFailed = True
            continue

        _add_to_dict(sName, dEntry, dCards)
    if bFailed:
        return None

    return dCards


def format_lib_card(dEntry):
    """Return a formatted string for a library card"""
    aParts = [ "Name: %s" % dEntry['Name']]
    if 'AKA' in dEntry:
        aParts.append("AKA: %s" % dEntry['AKA'])
    aParts.append("[%s]" % dEntry['Set'])
    aParts.append("Cardtype: %s" % dEntry['Type'])
    for sField in ['Clan', 'Creed', 'Cost', 'Discipline', 'Virtue']:
        if sField in dEntry:
            aParts.append('%s: %s' % (sField, dEntry[sField]))

    if 'Burn Option' in dEntry:
        aParts.append('Burn Option')

    if 'Banned' not in dEntry:
        aParts.append(dEntry['Text'])
    else:
        aParts.append('%s %s' % (dEntry['Text'], dEntry['Banned']))

    aParts.append("Artist: %s" % dEntry['Artist'])

    return '\n'.join(aParts)


def format_crypt_card(dEntry):
    """Return a formatted string for a crypt card"""
    aParts = ["Name: %s" % dEntry['Name']]
    if 'AKA' in dEntry:
        aParts.append("AKA: %s" % dEntry['AKA'])
    aParts.append("[%s]" % dEntry['Set'])
    aParts.append("Cardtype: %s" % dEntry['Type'])
    if 'Creed' in dEntry:
        aParts.append('Creed: %s' % dEntry['Creed'])
    else:
        aParts.append('Clan: %s' % dEntry['Clan'])
    for sField in ['Group', 'Capacity', 'Life', 'Virtue', 'Discipline']:
        if sField in dEntry:
            aParts.append('%s: %s' % (sField, dEntry[sField]))

    if 'Banned' not in dEntry:
        aParts.append(dEntry['Text'])
    else:
        aParts.append('%s. %s' % (dEntry['Text'], dEntry['Banned']))

    aParts.append("Artist: %s" % dEntry['Artist'])

    return '\n'.join(aParts)


class PmWikiReportFormatter(object):
    def __init__(self, playtesters):
        self.playtesters = playtesters

    def format_lib_card(self, dEntry):
        aSummaryParts = [dEntry['Type']]
        for sField in ('Cost', 'Clan', 'Creed'):
            if dEntry.get(sField):
                aSummaryParts.append(dEntry[sField])
        if dEntry.get('Burn Option'):
            aSummaryParts.append("Burn Option")

        aParts = [
            "!!!%s" % dEntry['Name'],
            "",
            "''%s'' \\\\" % ", ".join(aSummaryParts),
            "''%s''" % " ".join(dEntry['Text'].splitlines()),
            "",
        ]
        aParts.extend("* %s:" % name for name in self.playtesters)
        return '\n'.join(aParts)

    def format_crypt_card(self, dEntry):
        aSummaryParts = []
        for sField in ('Clan', 'Creed'):
            if dEntry.get(sField):
                aSummaryParts.append(dEntry[sField])
        if 'Capacity' in dEntry:
            aSummaryParts.append("%s cap" % dEntry['Capacity'])
            aSummaryParts.append("%s grp" % dEntry['Group'])
            aSummaryParts.append(dEntry['Discipline'])
        else:
            aSummaryParts.append("%s life" % dEntry['Life'])
            aSummaryParts.append("%s grp" % dEntry['Group'])
            aSummaryParts.append(dEntry['Virtue'])

        aParts = [
            "!!!%s" % dEntry['Name'],
            "",
            "''%s'' \\\\" % ", ".join(aSummaryParts),
            "''%s''" % dEntry['Text'],
            "",
        ]
        aParts.extend("* %s:" % name for name in self.playtesters)
        return '\n'.join(aParts)
