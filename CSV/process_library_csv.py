#!/usr/bin/python3

import sys

from sutekh import start

from vtes_csv import read_lib, format_lib_card

start()

dCards = read_lib(sys.argv[1])

for sName in sorted(dCards):
    print(format_lib_card(dCards[sName]))
    print()
