#!/usr/bin/python3

import sys

from sutekh import start

from vtes_csv import read_crypt, format_crypt_card

start()

dCards = read_crypt(sys.argv[1])

for sName in sorted(dCards):
    print(format_crypt_card(dCards[sName]))
    print()
