#!/usr/bin/python3

import sys

from sutekh import start

from vtes_csv import read_crypt, PmWikiReportFormatter

start()

formatter = PmWikiReportFormatter(["Player 1", "Player 2", "Player 3"])
dCards = read_crypt(sys.argv[1])

for sName in sorted(dCards):
    print(formatter.format_crypt_card(dCards[sName]))
    print()
